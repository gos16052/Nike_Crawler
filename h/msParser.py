import re
import msFileIO as m
from msTree import msTree

class msParser:
	def __init__(self, html):
		self.html = html
		self.root = msTree({})

#		self.cutComment()
		self.cutScript()
		self.cutStyle()
		self.cutNoscript()
		self.cutDOCTYPE()
		self.tagsToTree()

		m.save('test.txt', self.html)

	def cutComment(self):
		self.html = re.sub('(<!--(?:.|\s)*?-->)', '', self.html)

	def cutScript(self):
		self.html = re.sub('(<script(?:.|\s)*?>(?:.|\s)*?</script>)', '', self.html)

	def cutNoscript(self):
		self.html = re.sub('(<script(?:.|\s)*?>(?:.|\s)*?</script>)', '', self.html)

	def cutStyle(self):
		self.html = re.sub('(<style(?:.|\s)*?>(?:.|\s)*?</style>)', '', self.html)

	def cutDOCTYPE(self):
		self.html = re.sub('<!DOCTYPE html>', '', self.html)

	def tagsToTree(self):
		myTree = self.root
		for m in re.finditer('(<(?P<name>/?\w*){1}\s?.*?>(?P<text>[^<]*?)<)', self.html):
			_dict = { 'name' : m.groupdict()['name'] }

			text = m.groupdict()['text']
			if len(re.sub('\s', '', text)):
				_dict['text'] = re.sub('\t|\n', '', text)

			o = m.group()
			o = re.sub('<'+m.groupdict()['name']+' ?', '', o)
			for t in re.finditer('(?P<key>[A-Za-z]*?)="(?P<value>.*?)" ?', o):
				_dict[t.group(1)] = t.group(2)
			if re.findall('/>',o):
				_dict['end'] = True
			else:
				_dict['end'] = False

			print _dict

			if _dict['name'] == 'html': #root
				self.root.dict = _dict
#				print _dict
				continue
			else:
				myTree.setChild(_dict)

			if re.match('/', _dict['name']):
#				print _dict
#				print myTree.depth
				if _dict['name'] == myTree.children[len(myTree.children)-1].dict['name']:
#					print myTree.depth
					continue
				while _dict['name'] != myTree.dict['name']:
					myTree = myTree.parent
#				print myTree.depth

			if _dict['end'] == False:
				if len(myTree.children)==0:
					continue
				myTree = myTree.children[len(myTree.children)-1]
			if _dict['name'] == '/body':
				print myTree.depth
#		self.travel(self.root)

	def travel(self, node):
		print node.dict
		print node.depth
		for child in node.children:
			self.travel(child)



if __name__ == '__main__':
	msParser(m.read('.cache/.displayShopCache/NK1A41C38.html'))
