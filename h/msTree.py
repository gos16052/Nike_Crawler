class msTree:
	def __init__(self, _dict):
		self.dict = _dict;
		self.children = []
		self.parent = self # parent root = root 
		self.depth = 0

	def setChild(self, _dict):
		child = msTree(_dict)
		self.children.append(child)
		self.children[len(self.children)-1].parent = self
		self.children[len(self.children)-1].depth = self.depth + 1
