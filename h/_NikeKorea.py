import re
from urllib import urlopen
import httplib2
from selenium import webdriver
import sys
from time import localtime, strftime
import os
import types

CACHE = '.cache/'
href = '?displayNo='
reload(sys)
sys.setdefaultencoding('utf-8')
URLS = {
	'men' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A20A07&LReco=Y&allYn=Y&storeNo=2&siteNo=14218",
	'women' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A21A07&LReco=Y&storeNo=2&siteNo=14218",
	'kids' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A37&storeNo=2&siteNo=14218",
	'launchCalender' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A41&storeNo=2&siteNo=14218"
}

def getDay():
	return strftime('%y%m%d', localtime())

def main():
	urlsCalenderR = getUrlsCalenderR('/home/ms/Nike.html')
#	urlsCalenderU = getUrlsCalenderU()
	g = GetData(urlsCalenderR)

"""get database"""
class GetData:
	def __init__(self, urls):
		self.products = []
		if not os.path.isdir(CACHE + '.displayShopCache'):
			os.mkdir(CACHE + '.displayShopCache')
		for url in urls:
			if re.findall('.*?/displayShop.lecs', url):
				self.displayShop(url)
			if re.findall('.*?/displayShopCache.lecs', url):
				self.displayShopCache(url)
		# for product in self.products:
		# 	MyPrettyPrinter().pprint(product)


	def displayShop(self, url):
		' '

	def isManyProducts(self, html):
		nums = ["",]
		for a in re.finditer('(\d{6}-\d{3})', html):
			if a.group() in nums:
				continue
			else:
				nums.append(a.group())
		return [len(nums)>2,nums]


	def displayShopCache(self, url):
		path = CACHE + '.displayShopCache/'
		fname = re.findall('NK1A41.{3}', url)[0]

#		if re.findall('NK1A41C28', url): return
		if re.findall('NK1A41C25', url): return #jordan week : retro products
#		if re.findall('NK1A41B96', url): return
#		if re.findall('NK1A41B95', url): return
#		if re.findall('NK1A41B70', url): return
#		if re.findall('NK1A41B71', url): return
#		if re.findall('NK1A41B72', url): return
#		if re.findall('NK1A41B68', url): return
		if re.findall('NK1A41B65', url): return
		if re.findall('NK1A41B67', url): return
		if re.findall('NK1A41B64', url): return
		if re.findall('NK1A41B62', url): return
		if re.findall('NK1A41B60', url): return
		if re.findall('NK1A41B59', url): return

		print re.findall('NK1A41...', url)
		if 1:#re.findall('NK1A41C38', url):
			if os.path.isfile(path + fname + '.html'):
				f = open(path + fname + '.html', 'r')
				html = f.read()
			else:
				f = open(path + fname + '.html', 'w')
				driver = webdriver.Firefox()
				driver.get(url)
				html = driver.page_source.encode('utf-8')
				print html
				driver.close()
				f.write(html)
				f.close()
			product = {}


			buy = re.findall('a class="btn-type1 btn-black" id="lc_link." href="(.*?goodsNo.*?)"'.encode('utf-8'), html)
			if len(buy)>0:
				if self.isManyProducts(html)[0]:
					return self.displayShopCache_manyproducts(path+fname+'.html', url)
#				print buy[0] # link to buy item
				return
				"""go goods"""""""""""""""""""""""""""""""""""""""""""""

			""" many products"""
			isis = self.isManyProducts(html)
			if isis[0]:
				return self.displayShopCache_manyproducts(path+fname+'.html', url)
			product['number'] = isis[1]

			""" about info """
#			info = re.search('lcbv_pro_info">.*?\s?.*?<[p|P]>(.*?)\s?.*?</d', html).group()
			info = re.findall('lcbv_pro_info">.*?\s?.*?(?:<[p|P]>(.*?)</[p|P]>)(?:<[p|P]>(.*?)</[p|P]>)?', html)
			for tup in info: 
				for r in tup:
					if re.findall('(\d{6}-\d{3},)',r): # N product's'
						return self.displayShopCache_manyproducts(path+fname+'.html', url)


#			print url #saved url
			product['url'] = url

			date =  re.search('lcbv_pro_desc">\n?\s*(?:<p>)?(.*)', html).group(1) #released date
			product['date'] = date

#			print re #type of product
			product['type'] = ''

			img = re.search('(http://.*?(\d{6}-\d{3}).png)', html)
#			print img.group(2) #product code
			product['number'] = img.group(2)
			self.products.append(product)

#			print img.group(1) #img url
			product['img'] = img.group(1)

#			print re.search('lcbv_pro_info">.*?\s?.*?<[p|P]>(.*?)<',html).group(1) #product color
			product['color'] = re.search('lcbv_pro_info">.*?\s?.*?<[p|P]>(.*?)<',html).group(1)


#			print re.search('lcbv_pro_title">.*?\s?.*?<[p|P]>(.*?)<',html).group(1) #product name
			product['name'] = re.search('lcbv_pro_title">.*?\s?.*?<[p|P]>(.*?)<',html).group(1)

#			print re.search('lcbv_pro_price">(.*?)<',html).group(1) #retail price
			product['price'] = re.search('lcbv_pro_price">(.*?)<',html).group(1)

#			for r in product: print r + ' : ' + product[r]
	
	def displayShopCache_manyproducts(self, path, url):
		print url
		f = open(path, 'r')
		html = f.read().encode('utf-8')
		f.close()
		date =  re.search('lcbv_pro_desc">\n^\s*(.*)', html, re.M).group(1) #released date
		pattern = """	
		g>(?P<name>.*?)</strong></p>\s*?
		<p>\s*?(?P<number>\d{6}-\d{3})</p>(?:.*?)\s*?(?:.*?)
		<p>\s*?(?P<color>.*?)</p>(?:.*?)\s*?(?:.*?)
		<p>\s*?(?P<price>.*?)</p>(?:.*?)(\s*?.*?)*?
		lc_view_product(?:.*?)\s*?(?:.*?)	
		<img[ ]src="(?P<img>.*?)"
		"""
		if re.findall('[B70]|[B68]', url):
			pattern = """	
			g>(?P<name>.*?)</strong></p>\s*?
			<p>\s*?(?P<number>\d{6}-\d{3})</p>(?:.*?)\s*?(?:.*?)
			<p>\s*?(?P<color>.*?)</p>(?:.*?)\s*?(?:.*?)
			<p>\s*?(?P<price>.*?)</p>(?:.*?)(\s*?.*?)*?
			lc_view_product(?:.*?)\s*?(?:.*?)	
			<img[ ]src="(?P<img>.*?)"
			"""
		product= {}
		resultList = [m.groupdict() for m in re.finditer(pattern, html, re.X|re.I)]
		MyPrettyPrinter().pprint(resultList)
		for resultDict in resultList:
			for name in resultDict:
				print ''#resultDict[name]
			product['url'] = url
			product['type'] = ''
			product['date'] = date
			self.products.append(product)
	#end
#end

""" 	USE print
	MyPrettyPrinter().pprint(var)
	"""
import pprint
class MyPrettyPrinter(pprint.PrettyPrinter):
	def format(self, _object, context, maxlevels, level):
		if isinstance(_object, unicode):
			return "'%s'" % _object.encode('utf8'), True, False
		elif isinstance(_object, str):
			_object = unicode(_object,'utf8')
			return "'%s'" % _object.encode('utf8'), True, False
		return pprint.PrettyPrinter.format(self, _object, context, maxlevels, level)

""" get will released product """
def getUrlsCalenderU():
	url = URLS['launchCalender']
	driver = webdriver.PhantomJS()#Firefox()
	driver.get(url)
	_html = driver.page_source.encode('utf-8')
	reqRead = _html.encode('utf-8')
	driver.close()

	time = getDay()
	f = open('CalenderU ' + time + '.html', 'w')
	f.write(reqRead)
	ret = re.findall('(http://www.nike.co.kr/display/displayShop\.lecs\?displayNo=\w+)', reqRead)
	return { 'Cache' : ret }

"""get already released product"""
def getUrlsCalenderR(nikeHtmlPath):
	f = open(nikeHtmlPath, 'r')
	read = f.read()
	Cache = re.findall('(http://www.nike.co.kr/display/displayShop.{0,5}\.lecs\?displayNo=\w+)', read)
	return Cache

"""get last redirected url"""
def getContentLocation(link):
	h = httplib2.Http(".cache_httplib")
	h.follow_all_redirects = True
	resp = h.request(link, "GET")[0]
	contentLocation = resp['content-location']
	return contentLocation

if __name__ == "__main__":
	main()