def save(path, text):
	write(open(path, 'w'), text, 1)

def write(f, text, isClose):
	f.write(text)
	if isClose:
		f.close()

def read(path):
	f = open(path, 'r')
	read = f.read()
	f.close()
	return read