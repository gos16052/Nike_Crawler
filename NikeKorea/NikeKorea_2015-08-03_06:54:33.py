import re
from urllib import urlopen
import httplib2
from selenium import webdriver
import sys
from time import localtime, strftime
import os
import types
import pickle

CACHE = '.cache/'
href = '?displayNo='
reload(sys)
sys.setdefaultencoding('utf-8')
URLS = {
	'men' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A20A07&LReco=Y&allYn=Y&storeNo=2&siteNo=14218",
	'women' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A21A07&LReco=Y&storeNo=2&siteNo=14218",
	'kids' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A37&storeNo=2&siteNo=14218",
	'launchCalender' : "http://www.nike.co.kr/display/displayShopCache.lecs?displayNo=NK1A41&storeNo=2&siteNo=14218"
}

def getDay():
	return strftime('%y%m%d', localtime())

def main():
	urlsCalenderR = getUrlsCalenderR('/home/ms/Nike.html')
#	urlsCalenderU = getUrlsCalenderU()
	g = GetData(urlsCalenderR)

"""get database"""
class GetData:
	def __init__(self, urls):
		if os.path.isfile('.db'):
			f = open('.db','r')
			self.products = pickle.load(f)
			f.close()
		else:
			self.products = []

		if not os.path.isdir(CACHE + '.displayShopCache'):
			os.mkdir(CACHE + '.displayShopCache')
		for url in urls:
			if re.findall('.*?/displayShop.lecs', url):
				self.displayShop(url)
			if re.findall('.*?/displayShopCache.lecs', url):
				self.displayShopCache(url)

		f = open('.db', 'w')
		pickle.dump(self.products, f)
		f.close()

		# for product in self.products:
		# 	MyPrettyPrinter().pprint(product)

	def displayShop(self, url):
		' '

	def isManyProducts(self, html):
		nums = ["",]
		for a in re.finditer('(\d{6}-\d{3})', html):
			if a.group() in nums:
				continue
			else:
				nums.append(a.group())
		return [len(nums)>2,nums]

	def remainProArea(self, html):
		return re.findall("""(?x)
			(<div[ ]class="lcv_pro_area">[\s\S]*?)
			(?:<!--[ ]//end[ ]lcv_pro_area)
			""", html)[0]

	def cutTagP(self, html):
		html = re.sub('</?[bB][rR]\s*?/?>', '', html)
		return re.sub('</?[p|P]>', '', html)

	def cutComment(self,html):
		return re.sub('<!--[\s\S]*?-->', '', html)

		#cut body text about explain
	def cutBodyText(self,html):
		return re.sub('<?.*?>?\s*?.*?\s*?.*?\.\s', '\n', html)

	def displayShopCache(self, url):
		path = CACHE + '.displayShopCache/'
		fname = re.findall('NK1A41.{3}', url)[0]

 		if re.findall('NK1A41C25', url): return #jordan week : retro products

		print re.findall('NK1A41...', url)

		if os.path.isfile(path + fname + '.html'):
			f = open(path + fname + '.html', 'r')
			html = f.read()
			f.close()
			
			f = open(path + fname + '.html', 'w')
			f.write(self.cutTagP(html))
			f.close()
		else:
			f = open(path + fname + '.html', 'w')
			driver = webdriver.Firefox()
			driver.get(url)
			html = self.remainProArea(driver.page_source.encode('utf-8'))
			html = self.cutTagP(html)
			driver.close()
			f.write(html)
			f.close()
		

		date =  re.search('lcbv_pro_desc">\n?\s*(.*)', html).group(1) #released date

		buy = re.findall('a class="btn-type1 btn-black" id="lc_link." href="(.*?goodsNo.*?)"'.encode('utf-8'), html)
		if len(buy)>0:
			if self.isManyProducts(html)[0]:
				return self.displayShopCache_manyproducts(path+fname+'.html', url, date)
#				print 'BUY Button'
			return
			"""go goods page"""""""""""""""""""""""""""""""""""""""""""""

		#numbers = re.findall('(\d{6}-\d{3},)',html)
		

		""" many products"""
		isis = self.isManyProducts(html)
		if isis[0]:
			return self.displayShopCache_manyproducts(path+fname+'.html', url, date)

		product = {}	
		product['number'] = isis[1]

		""" about info """
		info = re.findall('lcbv_pro_info">.*?\s?.*?', html)
		for tup in info: 
			for r in tup:
				if re.findall('(\d{6}-\d{3},)',r): # N product's'
					return self.displayShopCache_manyproducts(path+fname+'.html', url, date)
		product['date'] = date
		product['url'] = url
		product['type'] = ''
		product['number'] = re.search('(http://.*?(\d{6}-\d{3}).*?.png)', html).group(2)
		product['img'] = re.findall('(http://.*?(\d{6}-\d{3}).*?.png)', html)
		product['color'] = re.search('lcbv_pro_info">.*?\s*(.*?)(?:\d{6}-\d{3})*?\s*?<',html).group(1)
		product['name'] = re.search('lcbv_pro_title">.*?\s*(.*?)\s*<',html).group(1)
		product['price'] = re.search('lcbv_pro_price">(.*?)<',html).group(1)

		self.products.append(product)
#		MyPrettyPrinter().pprint(product)

	def displayShopCache_manyproducts(self, path, url, date):
		f = open(path, 'r')
		html = f.read().encode('utf-8')
		f.close()

		date =  re.search('lcbv_pro_desc">\n^\s*(.*)', html, re.M).group(1) #released date
		html = self.cutBodyText(html)
		html = re.sub('12px"><strong>.*?</strong>', '', html) #sold out text
		html = re.sub('\n\s*?\n', '\n', re.sub('\n\s*?\n', '\n', html))

		parse = re.findall("""
			<strong>[^</strong>]*?</strong> #name
			[\s\S]*?(?:<span.*?)?[\s\S]*?(?:</span>)?(?:[\s\S]*?<)
			""", html, re.X)
		for p in parse:
			findList = re.findall(r"""
				(?:[\n\t]+)(.*)
				|(?:\<strong)(>.*?)(?:</strong>)
				""", p, re.X)
			product = {}
			product['date'] = date
			product['url'] = url
			for findTuple in findList:
				for findElement in findTuple:
					if len(findElement)>2:
						m = re.match('>(.*)',findElement)
						if m: 
							product['name'] = m.group(1)
							continue
						m = re.match('\d{6}-\d{3}', findElement)
						if m:
							product['number'] = m.group()
							continue
						m = re.match("\d{2,3},\d{3}", findElement)
						if m:
							product['price'] = m.group()
							continue
						m = re.match(".*", findElement)
						if m:
							product['color'] = m.group()
							continue
			regex = '(?:src=")(http://.*?'+product['number']+'.*?)"'
			imgs = re.findall(regex, html)
			if len(imgs):
				product['img'] = imgs 
			else:
				product['img'] = self.products[len(self.products)-1]['img']
			product['type'] = ''
			self.products.append(product)
	#end
#end

""" 	USE print
	MyPrettyPrinter().pprint(var)
	print MyPrettyPrinter().pformant(var)
"""
import pprint
class MyPrettyPrinter(pprint.PrettyPrinter):
	def format(self, _object, context, maxlevels, level):
		if isinstance(_object, unicode):
			return "'%s'" % _object.encode('utf8'), True, False
		elif isinstance(_object, str):
			_object = unicode(_object,'utf8')
			return "'%s'" % _object.encode('utf8'), True, False
		return pprint.PrettyPrinter.format(self, _object, context, maxlevels, level)

""" get will released product """
def getUrlsCalenderU():
	url = URLS['launchCalender']
	driver = webdriver.PhantomJS()#Firefox()
	driver.get(url)
	_html = driver.page_source.encode('utf-8')
	reqRead = _html.encode('utf-8')
	driver.close()

	time = getDay()
	f = open('CalenderU ' + time + '.html', 'w')
	f.write(reqRead)
	ret = re.findall('(http://www.nike.co.kr/display/displayShop\.lecs\?displayNo=\w+)', reqRead)
	return { 'Cache' : ret }

"""get already released product"""
def getUrlsCalenderR(nikeHtmlPath):
	f = open(nikeHtmlPath, 'r')
	read = f.read()
	Cache = re.findall('(http://www.nike.co.kr/display/displayShop.{0,5}\.lecs\?displayNo=\w+)', read)
	return Cache

"""get last redirected url"""
def getContentLocation(link):
	h = httplib2.Http(".cache_httplib")
	h.follow_all_redirects = True
	resp = h.request(link, "GET")[0]
	contentLocation = resp['content-location']
	return contentLocation

if __name__ == "__main__":
	import cProfile
	cProfile.run('main()')
	# main()
