"""
	version management tool
	: periodically save the file you want
"""
from time import gmtime, strftime, sleep
from shutil import copy
import os
import re
import sys

"""
	argv[1] : file path that need to has extension name
		  ex) h/NikeKorea.py	O
			  h/NikeKorea		X
	argv[2] : period to save(sec)
"""
def main(argv):
	print 'You have to remember pid : ' + str(os.getpid())
	path = re.split('\.|/',argv[1])
	ext = path[len(path)-1]
	name = path[len(path)-2]
	os.popen('subl '+argv[1]+' &')
	if not os.path.isdir(name):
		os.mkdir(name)
	print argv[1]
	while 1:
		sleep(int(argv[2]))
		pathname = name + '/' + name + '_' + strftime("%Y-%m-%d_%H:%M:%S", gmtime()) + '.' + ext
		copy(argv[1], pathname)
#		os.popen('cp '+argv[1]+' '+filename)
	return 

if __name__=='__main__':
	if len(sys.argv)==3:
		main(sys.argv)
	print 'wrong command!!!!'
	print 'plz typing a command like : python subl.py filepath savePeriod(sec)'
