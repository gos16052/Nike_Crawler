(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	//비회원일 경우 ''
	var memberNo = '';	  // 회원번호 15자리 mbr_no
	var gender ='';		// M , F gen_sct_cd
	var age ='';			// 나이 ( 현재년도 – 생일년도) bday_dt
	
	try{
		$.ajax({
			type : "POST",
			url : "/display/chkLoginAjax.lecs",
			async : false,
			data:{"gaYn":"Y"},
			dataType : "html",
			success : function(data){
				if(data != 'N'){
					var temp = '';
					
					if(data.indexOf(';') > -1){
						temp = data.replace(/;/g,""); 
					}else{
						temp = data;
					}
					
					var eData = temp.split(":");
					init();
					memberNo = eData[0];
					gender = eData[1];
					age = eData[2];
				}
			}
		});
	}catch(e) {
		//에러나도 무시
	}		

	ga('create', 'UA-23010608-1', {'userId': memberNo},{'cookieDomain': 'nike.co.kr'});

	ga('set', 'dimension1', memberNo );		// 회원번호
	ga('set', 'dimension3', gender ); 		// 성별
	ga('set', 'dimension4', age ); 		// 나이

	ga('send', 'pageview');

	function init(){
		if('' != memberNo ){
			memberNo = '';
		}
		if('' != gender ){
			gender = '';
		}
		if('' != age ){
			age = '';
		}
	}