/****************************************/
/*	Name: Nike Store
/*	Part: display.js
/*	Version: 1.0
/*	Author: Tommy
/****************************************/

//Define Var.
var isRun = false;

//Notification
$(function() {
	var obj = $(".Notification .content .area ul");
	var size = $(".Notification .area ul li").size();
	var rightEventBtn = $(".Notification .right");
	var leftEventBtn =  $(".Notification .left");
	var m_id = 0;
	var intervalId;

	if(size < 2) {
		rightEventBtn.hide();
		return;
	} else {
		leftEventBtn.hide();
	}

	leftEventBtn.on("click", function(e){
		e.preventDefault();

		stopInterval();
		var id = m_id - 1;
		fn_renderSlide(obj, id, showEventBtn)
	})

	rightEventBtn.on("click", function(e){
		e.preventDefault();

		stopInterval();
		var id = m_id + 1;
		fn_renderSlide(obj, id, showEventBtn);
	})

	function fn_renderSlide(_obj, _id, _fnc) {
		if(isRun)
			return;

		isRun = true;

		_fnc(_id);
		var target = -(_obj.find("li").width() * _id);
		_obj.animate({
			marginLeft:target+"px"
		},
		{
			duration : 1000,
			queue : false,
			complete : function(){
				isRun = false;
			}
		});
	}
	function showEventBtn(_id) {
		var id = _id;

		if(id == size-1) {
			leftEventBtn.show();
			rightEventBtn.hide();
		} else if(id == 0) {
			leftEventBtn.hide();
			rightEventBtn.show();
		} else {
			leftEventBtn.show();
			rightEventBtn.show();
		}
		m_id = id;
	}

	function startInterval() {
		intervalId = setInterval(function(){
			var id = m_id + 1;
			if(id < size) {
				fn_renderSlide(obj, id, showEventBtn);
			} else {
				stopInterval();
			}
		}, 4500)
	}

	function stopInterval() {
		clearInterval(intervalId);
	}

	startInterval();


});




//Slideshow 스크립트
$(function() {
	var m_id = 0;
	var obj = $(".Slideshow .content .area ul");
	var leftBtn = $(".Slideshow .content .left-btn");
	var rightBtn = $(".Slideshow .content .right-btn");
	var dotBtn = $(".Slideshow .content .dot");
	var size = obj.find("li").size();

	if(size < 2) {
		leftBtn.hide();
		rightBtn.hide();
		dotBtn.hide();
		return;
	} else {
		leftBtn.hide();
	}

	leftBtn.on("click", function(e){
		e.preventDefault();
		var id = m_id - 1;
		fn_renderSlide(id);
	})

	rightBtn.on("click", function(e){
		e.preventDefault();
		var id = m_id + 1;
		fn_renderSlide(id);
	})

	dotBtn.find(">a").on("click", function(e){
		e.preventDefault();
		var id = $(this).index();
		if(m_id == id) return;
		fn_renderSlide(id);
	})

	function fn_renderSlide(_id) {
		if(isRun)
			return;
		isRun = true;
		showBtn(_id);

		var target = -(obj.find("li").width() * _id);
		obj.animate({
			marginLeft:target+"px"
		},
		{
			duration : 1000,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				isRun = false;
			}
		});
	}
	function showBtn(_id) {
		if(_id == size-1) {
			leftBtn.show();
			rightBtn.hide();
		} else if(_id == 0) {
			leftBtn.hide();
			rightBtn.show();
		} else {
			leftBtn.show();
			rightBtn.show();
		}
		dotBtn.find("a").eq(m_id).removeClass("on");
		dotBtn.find("a").eq(_id).addClass("on");
		m_id = _id;
	}
});



//Portrait-merch 스크립트
$(function() {
	$(".Portrait-merch .dot>a").hide();
	$(".Portrait-merch").each(function(n){
		$(this).attr("data-index", 0);
		var size = $(this).find(".content .area ul li").size();

		if(size>3) {
			for(var i =0; i <size-2; i++) {
				$(this).find(".content .dot>a").eq(i).show();
			}
		}
		$(this).attr("data-size", size);

		$(this).find(".content .left-btn").hide();
		if(size < 4) {
			$(this).find(".content .right-btn").hide();
			return;
		}
		$(this).find(".content .left-btn").on("click", function(e){
			e.preventDefault();
			var id = parseInt($(this).parents(".Portrait-merch").attr("data-index")) - 1;
			fn_renderSlide($(this).parents(".Portrait-merch"), id);
		})

		$(this).find(".content .right-btn").on("click", function(e){
			e.preventDefault();
			var id = parseInt($(this).parents(".Portrait-merch").attr("data-index")) + 1;
			fn_renderSlide($(this).parents(".Portrait-merch"), id);
		})

		$(this).find(".content .dot>a").on("click", function(e){
			e.preventDefault();

			var id = $(this).index();
			if(parseInt($(this).parents(".Portrait-merch").attr("data-index")) == id) return;
			fn_renderSlide($(this).parents(".Portrait-merch"), id);
		})
	})
	function fn_renderSlide(_obj, _id) {
		if(isRun)
			return;
		isRun = true;

		showBtn(_obj, _id);
		var target = -((_obj.find("li").width()+20) * _id);

		_obj.find("ul").animate({
			marginLeft:target+"px"
		},
		{
			duration : 1000,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				isRun = false;
			}
		});
	}
	function showBtn(_obj, _id) {
		if(_id == parseInt(_obj.attr("data-size"))-3) {
			_obj.find(".content .left-btn").show();
			_obj.find(".content .right-btn").hide();
		} else if(_id == 0) {
			_obj.find(".content .left-btn").hide();
			_obj.find(".content .right-btn").show();
		} else {
			_obj.find(".content .left-btn").show();
			_obj.find(".content .right-btn").show();
		}
		_obj.find(".dot a").eq(parseInt(_obj.attr("data-index"))).removeClass("on");
		_obj.find(".dot a").eq(_id).addClass("on");
		_obj.attr("data-index", _id);
	}
});


//Portrait-merch4 스크립트
$(function() {
	$(".Portrait-merch4 .dot>a").hide();
	$(".Portrait-merch4").each(function(n){
		$(this).attr("data-index", 0);
		var size = $(this).find(".content .area ul li").size();

		if(size>4) {
			for(var i =0; i <size-3; i++) {
				$(this).find(".content .dot>a").eq(i).show();
			}
		}
		$(this).attr("data-size", size);

		$(this).find(".content .left-btn").hide();
		if(size < 5) {
			$(this).find(".content .right-btn").hide();
			return;
		}
		$(this).find(".content .left-btn").on("click", function(e){
			e.preventDefault();
			var id = parseInt($(this).parents(".Portrait-merch4").attr("data-index")) - 1;
			fn_renderSlide($(this).parents(".Portrait-merch4"), id);
		})

		$(this).find(".content .right-btn").on("click", function(e){
			e.preventDefault();
			var id = parseInt($(this).parents(".Portrait-merch4").attr("data-index")) + 1;
			fn_renderSlide($(this).parents(".Portrait-merch4"), id);
		})

		$(this).find(".content .dot>a").on("click", function(e){
			e.preventDefault();

			var id = $(this).index();
			if(parseInt($(this).parents(".Portrait-merch4").attr("data-index")) == id) return;
			fn_renderSlide($(this).parents(".Portrait-merch4"), id);
		})
	})
	function fn_renderSlide(_obj, _id) {
		if(isRun)
			return;
		isRun = true;

		showBtn(_obj, _id);
		var target = -((_obj.find("li").width()+20) * _id);

		_obj.find("ul").animate({
			marginLeft:target+"px"
		},
		{
			duration : 1000,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				isRun = false;
			}
		});
	}
	function showBtn(_obj, _id) {
		if(_id == parseInt(_obj.attr("data-size"))-4) {
			_obj.find(".content .left-btn").show();
			_obj.find(".content .right-btn").hide();
		} else if(_id == 0) {
			_obj.find(".content .left-btn").hide();
			_obj.find(".content .right-btn").show();
		} else {
			_obj.find(".content .left-btn").show();
			_obj.find(".content .right-btn").show();
		}
		_obj.find(".dot a").eq(parseInt(_obj.attr("data-index"))).removeClass("on");
		_obj.find(".dot a").eq(_id).addClass("on");
		_obj.attr("data-index", _id);
	}
});


//p2-4개짜리
$(function(){
	$(".P2-4Touts .content>ul>li").each(function(n){
		$(this).find(".bg .txt").wordBreakKeepAll();
	})
})



//Carousel스크립트
$(function() {
	var m_id = 0;
	var obj = $(".carousel .content .listArea ul");
	var leftBtn = $(".carousel .content .prev");
	var rightBtn = $(".carousel .content .next");

	var size = obj.find("li").size();
	if(size < 3) {
		rightBtn.hide();
		return;
	} else {
		leftBtn.hide();
	}
	leftBtn.on("click", function(e){
		e.preventDefault();
		var id = m_id - 1;
		fn_renderSlide(id);
	})

	rightBtn.on("click", function(e){
		e.preventDefault();
		var id = m_id + 1;
		fn_renderSlide(id);
	})


	function fn_renderSlide(_id) {
		if(isRun)
			return;
		isRun = true;
		showBtn(_id);

		var target = -((obj.find("li").width()+20) * _id);
		obj.animate({
			marginLeft:target+"px"
		},
		{
			duration : 1000,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				isRun = false;
			}
		});
	}
	function showBtn(_id) {
		if(_id == size-3) {
			leftBtn.show();
			rightBtn.hide();
		} else if(_id == 0) {
			leftBtn.hide();
			rightBtn.show();
		} else {
			leftBtn.show();
			rightBtn.show();
		}
		m_id = _id;
	}
});



/*** Merch***/
$(function() {
	var id = 0;
	var $section = $(".Merch .content .section");
	var $btn = $(".Merch .content .section .btn a");
	$section.each(function(n){
		var size = $section.eq(n).find(".list li").size();
		if(size < 5) {
			$btn.parent().parent().find(".btn").eq(n).hide();
		}
	})
	$btn.on("mouseenter", function(e){
		e.preventDefault();
		var id = $(this).parent().parent().parent().index();
		var hei = $section.eq(id).find(".list").height()+50;
		$section.eq(id).find(".box").height(hei);
		$section.eq(id).addClass("active");
	})
	$section.on("mouseleave", function(e){
		$(this).find(".box").height(115);
		$(this).removeClass("active");
	})
});



$(function() {
	var obj;
	var tabBtn = $(".shop-this-look .tab-btn-area>a");
	var tabContent = $(".shop-this-look.type1 .tab-content");
	var duration = 1000;
	var set_id;
	tabBtn.on("click", function(e){
		e.preventDefault();

		if(isRun) return;

		isRun = true;
		var type ;
		if( $(this).parent().parent().hasClass("type1") ) {
			type = 1;
		} else if( $(this).parent().parent().hasClass("type2") ) {
			type = 2;
		}

		//$(this).parent().parent().attr("data-type");

		obj = $(".shop-this-look.type"+type);
		var target;
		if(obj.find(".tool-area").attr("data-expend").toUpperCase() == "YES"){
			target = 9;
			obj.find(".tab-btn-area>a").addClass("active");
		} else {
			if(type == 1) {
				target = 70;
			} else if(type == 2) {
				target = 272;
			}
			obj.find(".tab-content").show();
			obj.find(".tab-btn-area>a").removeClass("active");
		}

		obj.find(".tool-area").animate({
			height:target+"px"
		},
		{
			duration : duration,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){

				if(obj.find(".tool-area").attr("data-expend").toUpperCase() == "YES") {
					obj.find(".tab-content").hide();
				}
				if(obj.find(".tool-area").attr("data-expend").toUpperCase() == "YES") {
					obj.find(".tool-area").attr("data-expend", "NO")
					obj.find(".tab-btn-area>a").attr("alt", obj.find(".tab-btn-area>a").text()+" 열기");

				} else {
					obj.find(".tool-area").attr("data-expend", "YES")
					obj.find(".tab-btn-area>a").attr("alt", obj.find(".tab-btn-area>a").text()+" 닫기");
				}
				isRun = false;
			}

		});

	})
	tabContent.find("ul li a").on("mouseenter", function(){
		var a = $(this).parent().parent().parent().index();
		var b = $(this).parent().parent().index();
		set_id = setInterval(function(){
			tabContent.find("ul").eq(a).find("li").eq(b).find("span").fadeIn(200);
		}, 500)

	}).on("mouseleave", function(e){
		clearInterval(set_id);
		var a = $(this).parent().parent().parent().index();
		var b = $(this).parent().parent().index();
		tabContent.find("ul").eq(a).find("li").eq(b).find("span").fadeOut(200);
	})
});


/*** 나이키 커뮤니티 관련 스크립트 ***/
$(function() {
	var obj = $(".Social-feed .commu-div");

	var $socialPop = $(".socialFeed-pop");

	var isRun = false;
	var duration = 1000;
	var sId = 0;

	if(obj.find("ul").height() < 481) {
		obj.find(".btn").hide();
	}

	obj.find(".btn a").on("click", function(e){
		e.preventDefault();

		var isStatus = $(this).parent().parent().attr("data-expend");
		var target;

		if(isStatus.toUpperCase() == "NO") {
			target = $(".commu-box ul").height();
			$(this).parent().parent().attr("data-expend", "YES");
			$(this).find("span.right").text("닫기").append('<span class="arr up"></span>');
		} else {
			target = 490;
			$(this).parent().parent().attr("data-expend", "NO");
			$(this).find("span.right").text("더보기").append('<span class="arr"></span>');
		}

		obj.find(".commu-box .commu-area").animate({
			height:target+"px"
		},
		{
			duration : duration,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				isRun = false;

			}
		});
	})

	$(window).resize(function(){
		if(obj.find("ul").height() > 480) {
			obj.find(".btn").fadeIn(500);
			var isStatus = obj.attr("data-expend");
			if(isStatus == "YES") {
				var target =  obj.find(".commu-box ul").height();

				obj.find(".commu-box .commu-area").animate({
					height:target+"px"
				},
				{
					duration : duration,
					easing : "easeOutQuint",
					queue : false,
					complete : function(){
						isRun = false;
					}
				});
			}
		} else {
			obj.find(".btn").fadeOut(500);
			var target =  obj.find(".commu-box ul").height();
			obj.find(".commu-box .commu-area").animate({
				height:target+"px"
			},
			{
				duration : duration,
				easing : "easeOutQuint",
				queue : false,
				complete : function(){
					isRun = false;
				}
			});
		}
	});

	obj.find(".commu-area>ul>li>a").on("mouseenter", function(e){
		//var id = $(this).parent().index();
		$(this).find(".box").fadeIn(200);
	}).on("mouseleave", function(e){
		$(this).find(".box").fadeOut(200);
	}).on("click", function(e){
		//e.preventDefault();
		return;
		var id = $(this).parent().index();
		showDim();
		renderSocialPop(id);
	})

	$socialPop.find("ul li").each(function(n){
		$(this).attr("data-index", n);
	})

	function renderSocialPop(id) {

		$socialPop.attr("active-index", id);


		var size = $socialPop.find(".box>ul>li").size();
		$socialPop.fadeIn(200);
		renderSocialItem(id);



		$socialPop.find(".close").off().on("click", function(e){
			//e.preventDefault();
			return;
			hideDim();
			$socialPop.fadeOut(200);
			$socialPop.find("ul.box>li").eq(id).fadeOut(200);


			var eid = $socialPop.attr("active-index");
			var path = $socialPop.find("ul li").eq(eid).find("iframe").attr("src");

			$socialPop.find("ul li").eq(eid).find("iframe").attr("src", path+"?autoplay=0");
		});


		$socialPop.find(".right").on("click", function(e){
			//e.preventDefault();
			return;
			var id = sId + 1;
			if(id > size-1 ) {
				id =0;
			}

			renderSocialItem(id);
		})

		$socialPop.find(".left").on("click", function(e){
			//e.preventDefault();
			return;
			var id = sId - 1;
			if(id < 0 ) {
				id =size-1;
			}
			renderSocialItem(id);
		})

	}

	function renderSocialItem(id) {
		$socialPop.find(".box>ul>li").hide();
		$socialPop.find(".box>ul>li").eq(id).show();

		var hei = $socialPop.find(".right-section").eq(id).height();
		$socialPop.height(hei);
		$socialPop.find(".left-section").eq(id).height(hei-40);

		sId = id;
	}


});



/*** 나이키 LOOK BOOK관련 스크립트 ***/

var $lookBook;
var $lookBookPop;

$(function() {
	var duration = 300;
	var lookBook_id;
	var item_id;


	$lookBook = $(".lookbook");
	$lookBookPop = $(".lookbook-pop");

	$lookBook.find("ul li a").on("mouseenter", function(){
		$(this).find(".over").fadeIn(300);
	}).on("mouseleave", function(e){
		$(this).find(".over").fadeOut(300);
	}).on("click", function(e){
		e.preventDefault();

		lookBook_id = $(".lookbook").index($(this).parents(".lookbook"));
		item_id = $(this).parent().index();

		showDim();
		renderLookBookPop(lookBook_id, item_id);
	});
});

function renderLookBookPop(i, j) {

	var size = $lookBookPop.find("ul.box>li").size();


	$lookBookPop.fadeIn(200);

	renderItem(j);


	$lookBookPop.find(".close").on("click", function(e){
		hideDim();
		$lookBookPop.fadeOut(200);
		$lookBookPop.find("ul.box>li").eq(j).fadeOut(200);
	});

	$lookBookPop.find(".right").on("click", function(e){
		e.preventDefault();

		var id = item_id + 1;
		if(id > size-1 ) {
			id =0;
		}
		renderItem(id);
	})

	$lookBookPop.find(".left").on("click", function(e){
		e.preventDefault();

		var id = item_id - 1;
		if(id < 0 ) {
			id =size-1;
		}
		renderItem(id);
	})
}

function renderItem(id) {
	if(isRun) return;

	isRun = true;

	$lookBookPop.find("ul.box>li").hide();
	$lookBookPop.find("ul.box>li").eq(id).show();

	var target = $lookBookPop.find("ul.box>li .image img").eq(id).height();

	$lookBookPop.find(".area").animate({

		height:target,
		marginTop:($lookBookPop.height()/2) - (target/2)

	},
	{
		duration : 1000,
		easing : "easeOutQuint",
		queue : false,
		complete : function(){
			item_id = id;
			isRun = false;
		}
	});


}

$(function(){
	var obj = $(".Benefit .content");

	obj.find("ul li").each(function(n){
		$(this).height($(this).height());

		if($(this).find(".txt span").height() < 51) {
			$(this).find(".btn").hide();
		}

	})
	obj.find("ul li .box").on("mouseenter", function(e){
		var id = $(this).parent().index();
		var hei =obj.find("ul li").eq(id).find(".txt span").height();
		if(hei <50) {
			obj.find("ul li").eq(id).find(".txt").height(33);
		} else {
			obj.find("ul li").eq(id).find(".txt").height(hei);
			obj.find("ul li").eq(id).find(".bar").css({
				"padding-top":"30px"
			})
		}
		obj.find("ul li").eq(id).find(".box").addClass("active");


		obj.find("ul li").eq(id).find(".btn").hide();
	})
	obj.find("ul li .box").on("mouseleave", function(e){
		var id = $(this).parent().index();
		obj.find("ul li").eq(id).find(".txt").height(33);
		obj.find("ul li").eq(id).find(".box").removeClass("active");

		if($(this).find(".txt span").height() > 48) {
			obj.find("ul li").eq(id).find(".btn").show();
		}
		obj.find("ul li").eq(id).find(".bar").css({
				"padding-top":"50px"
			})

	})
});


function fn_startDrag(event, info) {
	//alert("start");
}

function fn_stopDrag(event, info) {
	//alert("stop");
}

function fn_updateDrag(event, info) {
	var id = $(this).attr("data-index");
	var offset = $(this).offset();
        var yPos = offset.top - 140;
        var img_content_hei = $imgBox.find(".img_content").eq(id).height();
        var y =  yPos/imgBoxHei * (   img_content_hei  - imgBoxHei  );
       /*
         my = num/Stage.height * (mc._height-Stage.height);
	현재높이의 퍼센트값(?) : num/Stage.height
	mc가 이동할 수 있는 거리 : mc._height - Stage.height
         */
	/* animation */

      	$imgBox.find(".img_content").eq(id).animate({
		top: -y + "px"
	},
	{
		duration : 1000,
		easing : "easeOutQuint",
		queue : false
	});

	/*direct*/
	/*
	$imgBox.find(".img_content").eq(id).css({
		top: -y + "px"
	});
	*/
}



$(function(){
	var $obj = $(".kms");
	var arrPosX = new Array(0,85,180,272,349,433, 520);
	var box_id;
	var div_id =0;

	$obj.find(".menuBtn .menu").on("click", function(e){
		e.preventDefault();
		var id = $(this).parent().index();    //몇번째 버튼인지 추출
		box_id = $(this).parents(".kms").attr("data-index");
		setSlider(id, $(this).text());

		$obj.find(".menuBtn").attr("data-selected", id);

		$obj.eq(box_id).find(".conArea").hide();
		$obj.eq(box_id).find(".conArea").eq(id).fadeIn(300);

		div_id = id;
	}).on("focus", function(){
		var id = $(this).parent().index();    //몇번째 버튼인지 추출
		box_id = $(this).parents(".kms").attr("data-index");
		setSlider(id, $(this).text());

		$obj.find(".menuBtn").attr("data-selected", id);

		$obj.eq(box_id).find(".conArea").hide();
		$obj.eq(box_id).find(".conArea").eq(id).fadeIn(300);

		div_id = id;
	});





	$obj.find(".section2 .stit>a").on("click", function(e){
		e.preventDefault();
		box_id = $(this).parents(".kms").attr("data-index");

		var id = $obj.eq(box_id).find(".menuBtn>li").eq(div_id).find(".section2 .stit>a").index(this);


		$(this).parent().find(">a").removeClass("on");
		$(this).parent().find(">ul").hide();

		$(this).parent().find(">a").eq(id).addClass("on");
		$(this).parent().find(">ul").eq(id).fadeIn(200);



	}).on("focus", function(){
		box_id = $(this).parents(".kms").attr("data-index");

		var id = $obj.eq(box_id).find(".menuBtn>li").eq(div_id).find(".section2 .stit>a").index(this);


		$(this).parent().find(">a").removeClass("on");
		$(this).parent().find(">ul").hide();

		$(this).parent().find(">a").eq(id).addClass("on");
		$(this).parent().find(">ul").eq(id).fadeIn(200);
	});


	function setSlider(id, text) {
		$obj.eq(box_id).find(".slider .center").text( text );

		var target = arrPosX[id];

		$obj.eq(box_id).find(".slider").animate({
			left: target + "px"
		},
		{
			duration : 1000,
			easing : "easeOutQuint",
			queue : false
		});
	}

});




$(function(){
	var $obj = $(".unit");

	$obj.find(">a").on("focus", function(e){
		$(this).parent().addClass("over")
	}).on("focusout", function(e){

	}).on("mouseleave", function(e){

		$(this).parent().removeClass("over")
	});

});


//액티비티 메뉴 스크립트
$(function(){
	var $actBtn = $(".activity_tit");

	$actBtn.find(".dropMenu").hide();

	$actBtn.find(".menu>li").each(function(n){
		var wid = $(this).width();
		$actBtn.find(".dropMenu").eq(n).width(wid - 2);
	})

	$actBtn.find(".menu>li>a").on("mouseenter", function(e){
		var id = $(this).parent().index();
		$actBtn.find(".dropMenu").eq(id).stop(false, true).slideDown(200);
	});

	$actBtn.find(".menu>li").on("mouseleave", function(e){
		$(this).find(".dropMenu").stop(false, true).slideUp(200);
	})
});


//카테고리 그룹 관련 상품 스크립트
$(function(){
	var $siloGroup = $(".listArea");
	$siloGroup.find(".thumb").attr("data-index",0);
	$siloGroup.find("a.prev").hide();




	$(".content").each(function(i){
		$(this).find(".listArea").each(function(j){
			$(this).find(">li").each(function(r){

				//alert( );

				if($(this).find(".thumb ul li").size() - 1  < 3) {
					$(this).find(".thumb a.next").hide();
				}
				var grade = $(this).find(".star .rating>span").text();
				$(this).find(".star .rating>span").width( grade * 12.8 );
			})
		})
	})


	$siloGroup.find(".thumbList ul li a").on("mouseenter", function(e){
		e.preventDefault();

		var id =$(this).parent().index();

		$(this).parents(".unit").find(".mainImg li").hide();
		$(this).parents(".unit").find(".mainImg li").eq(id).show();

		$(this).parents(".unit").find("#priceList div").each(function(n){
			$(this).css("display", "none");
//			$(this).hide();
		});
		$(this).parents(".unit").find("#priceList div").eq(id).css("display", "block");
//		$(this).parents(".unit").find("#priceList div").eq(id).show();
	})

	$siloGroup.find("a.prev").on("click", function(e){
		e.preventDefault();

		var id = parseInt($(this).parent().attr("data-index"))-1;
		renderMoveThumbList($(this).parent(), id)
	})

	$siloGroup.find("a.next").on("click", function(e){
		e.preventDefault();
		var id = parseInt($(this).parent().attr("data-index"))+1;
		renderMoveThumbList($(this).parent(), id);
	});

	function renderMoveThumbList($thumb, id) {
		if(isRun) return;
		isRun= true;
		$thumb.find(".prev").show();
		$thumb.find(".next").show();

		if(id < 1) {
			$thumb.find(".prev").hide();
		} else if( id > $thumb.find("ul li").size()-4 ) {
			$thumb.find(".next").hide();
		}
		var target = id * -62;
		$thumb.find("ul").animate({
			left: target
		},
		{
			duration : 300,
			easing : "easeOutQuint",
			queue : false,
			complete : function(){
				$thumb.attr("data-index", id);
				isRun = false;
			}
		});
	}

	$(window).resize(function(){
		renderListResize();
	});
	renderListResize();
})

function renderListResize() {

	var wid = $(this).width() - 220;


	/* 상품리스트 수정 201406 start */
	if(wid>1400) {
		$(".listBox.group").find(".listArea").each(function(n){
			$(this).find(">li").show();
		})
		$(".listBox.group").width(1190);
	} else if(wid>1070) {
		$(".listBox.group").find(".listArea").each(function(n){
			$(this).find(">li").eq(4).hide();
			$(this).find(">li").eq(3).show();
			$(this).find(">li").eq(2).show();
		})
		$(".listBox.group").width(970);
	} else if(wid>750) {

		$(".listBox.group").find(".listArea").each(function(n){
			$(this).find(">li").eq(4).hide();
			$(this).find(">li").eq(3).hide();
			$(this).find(">li").eq(2).show();
		})
		$(".listBox.group").width(750);
	}  else {
		$(".listBox.group").find(".listArea").each(function(n){
			$(this).find(">li").eq(4).hide();
			$(this).find(">li").eq(3).hide();
			$(this).find(">li").eq(2).hide();
		})
		$(".listBox.group").width(550);
	}

	//$(".proList .content").css({border:"1px solid red"})

	if(wid>600 && wid <= 850) {
		if(wid>500) {
			$(".proList .content").width(550);
		} else {
			$(".proList .content").width(500);
		}
	} else if(wid>850 && wid <= 1400) {
		if(wid>1100) {
			$(".proList .content").width(1000);
		} else {
			$(".proList .content").width(750);
		}
	} else if(wid>1400) {
		$(".proList .content").width(1200);
	} else {
		$(".proList .content").width(750);
	}
	/* 상품리스트 수정 201406 end */
}

var $filter;
var arrfilterSize = new Array(185, 373, 562, 751, 940);

$(function(){
	$filter = $(".filter");

	renderSearchTop();
})

function renderSearchTop() {
	$filter.find(">ul> li> .menu").each(function(n){
		$(this).attr("data-name", $(this).text());
	})

	var id = $filter.find(">ul>li").size()-1;
	$filter.css({width:arrfilterSize[id]})


	$filter.find(">ul> li> .menu").on("mouseenter", function(e){
		var id = $(this).parent().index();

		if( $(this).parent().hasClass("on") ) {
			return;
		}

		$(this).parent().addClass("over");
		$(this).parent().on("mouseleave", function(e){

			$(this).removeClass("over");
		});
	})


	$filter.find(".release").on("click", function(e){

		$(this).parent().removeClass("on");
		$(this).parent().find(".menu").text(  $(this).parent().find(".menu").attr("data-name")  );
		$(this).hide();

		$(this).parent().find(".menu").on("mouseenter", function(e){
			var id = $(this).parent().index();

			$(this).parent().addClass("over");
			$(this).parent().on("mouseleave", function(e){
				$(this).removeClass("over");
			});
		})
	})


	$filter.find(".value ul li a").on("click", function(e){
		var txt =$(this).text();

		$(this).parents().parent().parent().parent().removeClass("over");
		$(this).parent().parent().parent().parent().addClass("on");

		$(this).parent().parent().parent().parent().find(".menu").text(txt);
		$(this).parent().parent().parent().parent().find(".release").show();

		$(this).parent().parent().parent().parent().find(".menu").off("mouseenter");
		$(this).parent().parent().parent().parent().find(".release").on("click", function(e){

			$(this).parent().parent().parent().removeClass("on");
			$(this).parents(".menu").text(  $(this).parents(".menu").attr("data-name")  );
			$(this).hide();

			$(this).parents(".menu").on("mouseenter", function(e){
				var id = $(this).parent().index();

				$(this).parent().addClass("over");
				$(this).parent().on("mouseleave", function(e){
					$(this).removeClass("over");
				});
			})
		})
	})
}

//LNB 작동 스크립트
$(function() {
	$("dl.att").each(function(n){
		$(this).attr("data-open","0");
		if($(this).find("ul li").size()>5){
			var txt = $("dl.att").eq(n).find("dt").text();
			 $("dl.att").eq(n).find("dd.view a span").removeClass("up");
			 $("dl.att").eq(n).find("dd.view a span").text(txt+" 더보기");

			$(this).find("dd.box").height(135);
			$(this).find("dd.view").on("click", function(e){
				e.preventDefault();
				if(isRun) return;
				isRun = true;
				if( $(this).parent().attr("data-open") == "0" ) {
					$("dl.att").eq(n).attr("data-open", "1");
					var txt = $("dl.att").eq(n).find("dt").text();
					 $("dl.att").eq(n).find("dd.view a span").addClass("up");
					 $("dl.att").eq(n).find("dd.view a span").text(txt+" 접기");

					$("dl.att").eq(n).find("dd.box").animate({
						height: 26 *$("dl.att").eq(n).find("dd.box ul li").size()
					},
					{
						duration : 300,
						easing : "easeOutQuint",
						queue : false,
						complete : function(){
							isRun = false;
							renderLNBPosition();
						}
					});
				} else {
					$("dl.att").eq(n).find("dd.view a span").removeClass("up");
					$("dl.att").eq(n).attr("data-open", "0");
					var txt = $("dl.att").eq(n).find("dt").text();
					 $("dl.att").eq(n).find("dd.view a span").text(txt+" 더보기");

					$("dl.att").eq(n).find("dd.box").animate({
						height: 135
					},
					{
						duration : 300,
						easing : "easeOutQuint",
						queue : false,
						complete : function(){
							renderLNBPosition();
							isRun = false;
						}
					});
				}
			})

		} else {
			$(this).find("dd.view").hide();
		}
	})
});


function renderPopEmail() {
	showDim();
	$(".email-pop").slideDown(200);
	$(".email-pop .close").on("click", function(e){
		e.preventDefault();
		$(".email-pop").slideUp(200);
		hideDim();
	})
}





$(function(){
	$(".group.type2 .b01").on("click", function(e){
		e.preventDefault();

		showDim();
		$("#aboutMVP").show();
		$("#aboutMVP").find(".close").on("click", function(e){
			e.preventDefault();
			hideDim();
			$("#aboutMVP").hide();
		})
	})

	$(".group.type2 .b02").on("click", function(e){
		e.preventDefault();
		showDim();
		$("#aboutMILE").show();
		$("#aboutMILE").find(".close").on("click", function(e){
			e.preventDefault();
			hideDim();
			$("#aboutMILE").hide();
		})
	})
})




var optionSize;
var optionId = 0;

function setOption(){
	optionSize = $(".option_area > ul > li").size();


	$(".option_btn_pre").hide();

	if(optionSize < 2) {
		$(".option_btn_next").hide();
	}

	$(".option_btn_pre").on("click", function(e){
		e.preventDefault();
		var id = optionId - 1;
		renderOptionSlide(id);
	});

	$(".option_btn_next").on("click", function(e){
		e.preventDefault();
		var id = optionId + 1;
		renderOptionSlide(id);
	});
}

function renderOptionSlide(id) {
	if(isRun) return;

	isRun = true;

	setOptionBtn(id);
	var target = -66 * id;

	$(".option_area>ul").animate({
		marginLeft: target
	},
	{
		duration : 300,
		easing : "easeOutQuint",
		queue : false,
		complete : function(){
			optionId = id;
			isRun = false;
		}
	});

}

function setOptionBtn(id) {
	$(".option_btn_pre").show();
	$(".option_btn_next").show()
	if(id == 0) {
		$(".option_btn_pre").hide();
	}
	if(id == (optionSize-5)) {
		$(".option_btn_next").hide();
	}
}


var d_height;
var w_height;
var d_top;
var position;
var isUp;

var up_gap = 70;
var down_gap = 21;

$(function(){
	$(window).scroll(function(){
		renderLNBPosition();
	});

	$(window).resize(function(){
		renderLNBPosition();
	});
	renderLNBPosition();
});

var inig = false;

function renderLNBPosition() {

	$(".lnbWrap.category").css({top: 163}); /* 20140307 추가 */

	if( $(".lnbWrap.category").size() == 0 )
		return;

	d_height = $(document).height(); // 다큐멘트 크기
	w_height = $(window).height();  // 브라우저 크기

	if(d_top > $(document).scrollTop() ) {
		isUp  =true;
	} else {
		isUp = false;
	}

	d_top = $(document).scrollTop(); // 다큐멘트 최상위 좌표
	position = $(window).scrollTop(); // 현재 스크롤바의 위치값을 반환합니다.


	if( $(".lnbWrap.category").height() > d_height - $(".footer").height() - 30 ) {

		$(".siloGroup").height(  $(".lnbWrap.category").height() );

		return;
	}

	if($(".lnbWrap.category").height() >  d_height - (50 + $(".footer").height() ) ) {

		return;
	}


	if( $(".lnbWrap.category").height()+200 <  w_height  ) {

		if( ( d_height - d_top - w_height) < $(".footer").height() - $(".lnbWrap.category").height() + down_gap  ) {

			return;
		}
		$(".lnbWrap.category").css({top: d_top+up_gap });

		if( $(".lnbWrap.category").offset().top > d_height-$(".footer").height() - $(".lnbWrap.category").height() ) {
			inig = true;

		} else {

			inig = false;
		}

		if( $(".lnbWrap.category").offset().top < 164 ) { /* 20140306 수정 */
			$(".lnbWrap.category").css({top:163}); /* 20140306 수정 */


		} else {
				if((d_height-d_top-w_height) < $(".footer").height()) {
					/*
					if(inig) return;

					inig = true;

					$(".lnbWrap.category").animate({
						top: d_height -  $(".lnbWrap.category").height() -  $(".footer").height() - down_gap
					},
					{
						duration : 300,
						easing : "easeOutQuint",
						queue : false,
						complete : function(){}
					});
					*/
					$(".lnbWrap.category").css({top: d_height -  $(".lnbWrap.category").height() -  $(".footer").height() - down_gap });
				} else {
					//$(".lnbWrap.category").css({top: (d_top +w_height) - $(".lnbWrap.category").height() - down_gap  });
				}
		}
		return;
	}

	if(isUp) {

		if( d_top < parseInt($(".lnbWrap.category").offset().top) - 163 ) { /* 20140306 수정 */
			$(".lnbWrap.category").css({top: d_top+up_gap });

		}

		if( $(".lnbWrap.category").offset().top < 164 ) { /* 20140306 수정 */

			$(".lnbWrap.category").css({top: 163 }); /* 20140306 수정 */

		}
		return;

	} else {

		if($(".lnbWrap.category").height()+$(".lnbWrap.category").offset().top-w_height > d_top) {
			//반응없는 구간

		} else {

			if( (d_top + w_height ) >  $(".lnbWrap.category").height() ) {
				if((d_height-d_top-w_height) < $(".footer").height()) {

					$(".lnbWrap.category").css({top: d_height -  $(".lnbWrap.category").height() -  $(".footer").height() - down_gap });
				} else {

					$(".lnbWrap.category").css({top: (d_top +w_height) - $(".lnbWrap.category").height() - down_gap  });
				}
			}  else {

				$(".lnbWrap.category").css({top: 163}); /* 20140306 수정 */

			}
		}
	}
}

/* 201406 lnb 추가 s */
$(function(){
	$(".nav_header .nav_tit a").toggle(toggleLnbTitleAcc, toggleLnbTitleAcc);

	function toggleLnbTitleAcc(){
		if( $("header .nav_action").hasClass("close") ) {
			$(this).parent().parent().parent().find(".nav_sec_wrap").slideDown(300);
			$(this).find(".nav_action").removeClass("close");
			$(".nav_header").css({
				"border-bottom":"1px solid #d2d2d8"
			});
		} else {
			$(this).parent().parent().parent().find(".nav_sec_wrap").slideUp(300);
			$(this).find(".nav_action").addClass("close");
			$(".nav_header").css({
				"border-bottom":"0"
			});
			$("header .nav_tit").css("padding","0 16px 17px 15px");
		}
	}

	$(".nav_sec_wrap a.tit").on("click", function(e){
		e.preventDefault();

		if( $(this).hasClass("close") ){
			$(this).removeClass("close");
			$(this).find(".nav_action").removeClass("close");
			$(this).parent().find(".nav_link_wrap").slideDown(300);
		} else {
			$(this).addClass("close");
			$(this).find(".nav_action").addClass("close");
			$(this).parent().find(".nav_link_wrap").slideUp(300);
		}

	});

	$(".nav_additional_con a").on("click", function(e){
		e.preventDefault();
		if( $(this).parent().hasClass("over") ){
			$(this).parent().parent().find("ul.more").slideUp(300);
			$(this).parent().removeClass("over");
			$(this).find("span").text("더보기");
		} else {
			$(this).parent().parent().find("ul.more").slideDown(300);
			$(this).parent().addClass("over");
			$(this).find("span").text("접기");
		}
	});
});

/* lnb 해상도에 따른 출력 형태 */
$(function(){
	/* 브라우저 리사이징 적용 시 주석 제거
	$(window).resize(function(){
		setLeftMenu();

	})/*.resize()*/
	setLeftMenu();

	function setLeftMenu() {

	var widthLnb = $(window).width();

		if (widthLnb < 1024) {

			$(".nav_sec_wrap").hide();
			$(".nav_link_wrap").hide();
			$(".nav_sec header").css("border","0");
			$("header .nav_tit").css("padding","0 16px 17px 15px");
			$("header .nav_action").addClass("close");
			$("section .tit, section .nav_action").addClass("close");

			//카테고리 영역 오픈
			$("section.first .tit, section.first .nav_action").removeClass("close");
			$("section.first .nav_link_wrap").show();

			//lnb 2depth 영역
			$(".nav_sec_wrap.depth2").show();
			$(".nav_sec_wrap.depth2 section a.tit").removeClass("close");
			$(".nav_sec_wrap.depth2 section .nav_action").removeClass("close");
			$(".depth2 .nav_link_wrap").show();
			$(".nav_header.depth2").css("border-bottom","1px solid #d2d2d8"); //20140723 추가

		}  else if (widthLnb < 1440) {

			$(".nav_sec_wrap").show();
			$(".nav_sec_wrap section a.tit").addClass("close");
			$(".nav_sec_wrap section.first a.tit").removeClass("close");
			$(".nav_sec_wrap section .nav_action").addClass("close");
			$(".nav_sec_wrap section.first .nav_action").removeClass("close");
			$(".nav_link_wrap").hide();
			$(".nav_sec_wrap .first .nav_link_wrap").show();

			//lnb 2depth 영역
			$(".nav_sec_wrap.depth2").show();
			$(".nav_sec_wrap.depth2 section a.tit").removeClass("close");
			$(".nav_sec_wrap.depth2 section .nav_action").removeClass("close");
			$(".depth2 .nav_link_wrap").show();
		}
		 else if (widthLnb < 1600) {
			//$(".nav_box").show();
		}
	}

	// lnb edepth 스크롤 20140723
	$(window).scroll(function(){

		if($("#depth2").height() > $(window).height()-160) {

			var winTop = $(window).scrollTop();
			var winHei = $("#depth2").height()-$(window).height()+160;

			if(winTop >= winHei){
				var gap =  parseInt($(window).scrollTop()*100 /( $(document).height() - $(".footer").height() - $(window).height()));
				if(gap > 98 ) {
					$("#depth2").css({
						position:"fixed",
						top:"auto",
						bottom:$(".footer").height()+20
					});
				} else {
					//이동중
					$("#depth2").css({
						position:"fixed",
						top:"auto",
						bottom:23
					});
				}
			} else {
				$("#depth2").css({
				       position:"absolute",
					top:23,
					bottom:"auto"
				});
			}
		} else {
			if($("#depth2").height() > $(window).height()-160) {

				var gap =  parseInt($(window).scrollTop()*100 /( $(document).height() - $(".footer").height() - $(window).height()));
				if(gap > 98 ) {
					$("#depth2").css({
						position:"fixed",
						top:"auto",
						bottom:$(".footer").height()+20
					});
				} else {
					//이동중
					$("#depth2").css({
						position:"fixed",
						top:"auto",
						bottom:$(".footer").height()+20
					});
				}
			} else {
				var gap =  parseInt($(window).scrollTop()*100 /( $(document).height() - $(".footer").height() - $(window).height()));
				//console.log(gap);
				if(gap > 98 ) {
					$("#depth2").css({
						position:"fixed",
						top:"auto",
						bottom:$(".footer").height()+20
					});
				} else {
					//이동중
					$("#depth2").css({
						position:"fixed",
						top:105,
						bottom:"auto"
					});
				}
			}
		}
	});

});
/* 201406 lnb 추가 e */