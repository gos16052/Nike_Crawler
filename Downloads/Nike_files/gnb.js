/****************************************/
/*	Name: Nike
/*	Part: gnb.js
/*	Version: 1.0
/*	Author: Tommy
/****************************************/

/* SWF Object 로드 */
document.write("<script src='../../com/js/swfobject.js'></script>");
/* 폰트 CROP스크립트 로드 */
document.write("<script src='../../com/js/jquery.word-break-keep-all.min.js'></script>");

//Define Var.
var logIn_status = false;
var $oneDepthLength;

var intervalId;
var selectedId = -1;
var arrSubBoxHei = {
             MEN:217,
             WOMEN:217,
             KIDS:217,
             SPORTS:217,
             MOS:96,
             RECENT:98,
             LOGIN:280,
             PROFILE:115,
             TOTAL:300,
             CART:260,
             INIT:0
}
var openSubName="";
var isSubExtend = false;
var isRun = false;
var du = 200;
var initHei = 135;
var profileWid;


function renderOneDepth() {
             $(".tier0 .facet>a").each(function(n){
                           $(this).bind("mouseenter", function(){
                                        oneDepthBtnOver(n);
                           }).bind("mouseleave", function(){
                                       oneDepthBtnOut(n);
                           }).bind("focus", function(){
                                        oneDepthBtnOver(n);
                           }).bind("focusout", function(){
                                        oneDepthBtnOut(n);
                           })
             });

             var main_touch;
             $(".gnav").bind("mouseenter", function(){
             		clearInterval(main_touch);
             })

             $(".gnav").bind("mouseleave", function(){
             		main_touch = setInterval(function(){
             			$(".tier0 .facet>a").removeClass("pushed");
                         	initialize();

             		}, 1000)

             })

             $(".subnav  .subnav-btn-area .subnav-btn").bind("click", function(e){
                           e.preventDefault();
                           var id = getSubIndex($(this).attr("data-subnav-label").toString());

                           if(isSubExtend) {
                                        initialize();
                           } else {
                                        //더보기
                                        renderSubExtend(id);
                                        $(this).text("닫기")
                                        isSubExtend = true;
                           }
             });


             $(".subnav-box a").bind("mouseenter", function(e){
                           $(this).stop().animate({
                                        "color":"#fa5400"
                           },
                           {
                                        duration : du,
                                        queue : false
                           })
             }).bind("mouseleave", function(e){
                           $(this).stop().animate({
                                        "color":"#333" //20140228 수정
                           },
                           {
                                        duration : du,
                                        queue : false
                           })
             })

         	/* 20140228 추가 s */
         	$(".subnav-box li a").bind("mouseenter", function(e){
         		$(this).stop().animate({
         			"color":"#fa5400"
         		},
         		{
         			duration : du,
         			queue : false
         		})
         	}).bind("mouseleave", function(e){
         		$(this).stop().animate({
         			"color":"#666"
         		},
         		{
         			duration : du,
         			queue : false
         		})
         	});
         	/* 20140228 추가 e */
}

function oneDepthBtnOver($id) {
             clearInterval(intervalId);

             $(".tier0 .facet>a").removeClass("pushed");
           //  initialize();


             $(".tier0 .facet").eq($id).find(".bg").stop().animate({
                           "opacity":1
             },
             {
                           duration : du,
                           queue : false
             });

             if(  $(".tier0 .facet").eq($id).find("span").hasClass("glyph-after") ) {
                           intervalId = setInterval(function(){
                                        if(openSubName != "") {
                                                     initialize();
                                        }

                                        $(".tier0 .facet>a").eq(selectedId).removeClass("pushed");
                                        $(".tier0 .facet>a").eq($id).addClass("pushed");

                                        renderTwoDepth(getNodeName($id));
                                        selectedId = $id;
                           },du);
             }
}

function initialize() {
             clearInterval(intervalId);
             getStyleTier(openSubName);

             $(".tier0").find(".subnav").eq(getSubIndex(openSubName)).hide();

             if(openSubName == "MOS") {
                           //
             }  else if(openSubName == "RECENT") {
                           //
             } else if(openSubName == "LOGIN") {
                           //
             } else if(openSubName == "PROFILE") {
                           //
             } else {
                           //renderSubSliding(initHei);
                           //$(".tier0").find(".subnav").eq(getSubIndex(openSubName)).find(".subnav-btn-area .subnav-btn").text("더보기");
                           //isSubExtend = false;
             }
             closeTwoDepth();
             openSubName ="";
}

function closeTwoDepth() {
             var target = 0;

             getStyleTier(openSubName);

             $(".tier1").stop().animate({
                           height: 0
             },
             {
                           duration : 300,
                           queue : false,
                           complete:function() {

                           }
             });

             $(".tier2").stop().animate({
                           height: 0
             },
             {
                           duration : 300,
                           queue : false,
                           complete:function() {

                           }
             });
}

function oneDepthBtnOut($id) {
             $(".tier0 .facet").eq($id).find(".bg").stop().animate({
                           "opacity":0
             },
             {
                           duration : du,
                           queue : false
             });
             clearInterval(intervalId);
}

function renderTwoDepth($name) {
             if(openSubName == $name)  {
                           return;
             }
             getStyleTier($name);
             openSubName = $name;

             if($name == "MEN")  {
                           renderSubExtend(1);
                           //$(this).text("닫기")
                           isSubExtend = true;
             } else if($name == "WOMEN") {
                           renderSubExtend(2);
                           //$(this).text("닫기")
                           isSubExtend = true;
             } else if($name == "KIDS") {
                           renderSubExtend(3);
                           //$(this).text("닫기")
                           isSubExtend = true;
             /* 20140228 s */
         	} else if($name == "JORDAN") {
         		renderSubExtend(7);
         		//$(this).text("닫기")
         		isSubExtend = true;
         	/* 20140228 e */
             } else if($name == "MOS"){
                           var target = arrSubBoxHei[$name];
                           clearInterval(intervalId);
                           $(".tier2").stop().animate({
                                        "height": target+"px"
                           },
                           {
                                        duration : du,
                                        queue : true,
                                        complete:function() {
                                                     renderSubMenu($name);
                                        }
                           });
             }  else {
                           var target = arrSubBoxHei[$name];

                           clearInterval(intervalId);

                           $(".tier1").stop().animate({
                                        "height": target+"px"
                           },
                           {
                                        duration : du,
                                        queue : true,
                                        complete:function() {
                                                     renderSubMenu($name);
                                        }
                           });


             }
             //renderSubExtend(1);
             //$(this).text("닫기")
                           //           isSubExtend = true;
}

function renderSubMenu($name) {
             $(".tier0").find(".subnav").eq(getSubIndex($name)).fadeIn(200);

}

function renderSubExtend($id) {
             if($id == 1) {
                           $id = 0;
             } else  if($id==2) {
                           $id = 1;
             } else if($id==3) {
                           $id =2;
         	} else if($id == 7) {
         					$id = 5;
             }


         	var maxLen = 0;
        	$(".subnav").eq($id).find(".subnav-box").each(function(n){

        		if( maxLen < $(this).find("ul li").size() ) {
        			maxLen = $(this).find("ul li").size()+1; //20140303 신상품이 있는 0번째 메뉴만  li size() 값에 1을 더함
        		}
        	})
        	var target = 85+ (17 * (maxLen)); // 20140303 gnb 간격조정


             renderSubSliding(target);
}

function renderSubSliding(target) {

             getStyleTier(openSubName);



             $(".tier0").find(".subnav .subnav-con .subnav-item .subnav-area").eq(getSubIndex(openSubName)).stop().animate({
                           "height": target+"px"
             },
             {
                           duration : du,
                           queue : false,
                           complete:function() {
                                        clearInterval(intervalId);
                                        $(".tier0").find(".subnav").eq(getSubIndex(openSubName)).fadeIn(200);
                           }

             });



             $(".tier1").stop().animate({
                           "height": (target+50)+"px" //20131202 2depth 간격조정
             },
             {
                           duration : du,
                           queue : false,
                           complete:function() {
					//
                           }
             });
}

function getNodeName($id) {
             return $(".tier0 .nav-section >a").eq($id).parent().attr("data-subnav").toUpperCase();
}

function getSubIndex($name) {
             var a;

             getStyleTier($name);

             $(".tier0").find(".subnav").each(function(n){
                           if($(this).attr("data-subnav-title").toUpperCase() == $name.toUpperCase()) {
                                        a = n;
                                        return;
                           }
             })

             return a;
}

function getStyleTier($name) {

	if($name == "MOS") {
		$(".tier2").css({
			top:"75px", // 20140303 수정
			left:"335px", //20140228 수정
			width:"378px"
		});
	} else if($name == "RECENT") {

		$(".tier1").css({
			top:"75px", // 20140303 수정
			right:"0px", // 20140228 수정
			width:"378px"
		});
	} else if($name =="LOGIN") {
		$(".tier1").css({
			top:"75px", // 20140303 수정
			right:-1,
			width:"296px"
		});
	} else if($name =="PROFILE") {
		profileWid = $(".tier0 .profile").width()+1;

		//프로필의 가로길이를 구해서 서브메뉴들의 위치를 재조정한다
		var w = $(".profile").width()
		$(".subnav.profile").width(w-25);

		$(".tier1").css({
			top:"75px", // 20140303 수정
			right:-2, // 20140303 수정
			width:profileWid
		});
	}  else {
		$(".tier1").css({
			top:"75px", // 20140303 수정
			right:0,
			width:"100%"
		});
	}
}

//카트담기가 되면 호출
function renderAddCart() {
	 /*
             $(".tier0 .facet>a").eq(8).addClass("pushed");
            */

            var cnt = 17  //카트 담은 갯수를 여기다 넣어주세요

            //$(".gnav .top-line .bubble .tip").text(cnt);
            //$(".gnav .top-line .bubble").show();


             $(".subnav.cart .close").bind("click", function(e){
                           $(".tier0 .facet>a").removeClass("pushed");
                           initialize();
             })

             var info_interval =setInterval(function(){
                     //      $(".tier0 .facet>a").removeClass("pushed");
                           initialize();
                           clearInterval(info_interval);
             }, 3000);

             renderTwoDepth("CART");
            // selectedId = 8;
}

//초기화

var searchStatus = 0;
var gnbTextStatus = 0;

$(function() {
             renderOneDepth();
             //renderAddCart();
             renderDepthAddBtn();

             var gen =  $(".gnav .left-area").width() + $(".gnav .right-area").width() + 70;

             //GNB의 min-width값을 지정(right-area가 밑으로 떨어지지 않는다)
             $(".gnav").css({
                           "min-width": gen+"px"
             });

             renderGNBResize();


             $(window).resize(function(){
                           renderGNBResize();
             });

			/*150127 : str*/
			var $research = $(".searchArea .research");
			var valueArr = $research.data("value");
			var valueSec = parseInt( $research.data("timer"), 10 );
			var valueCounter = 0;
			var valueTimer = setInterval(function(){
				autovalue();				
			}, valueSec);
			
			$research.val(valueArr[0]);
			
			function autovalue(){
				valueCounter++;
				valueCounter = valueCounter % valueArr.length;
				$research.val(valueArr[valueCounter]);
				$(window).data("nowsearchvalue", valueArr[valueCounter]);
			}
              /*150127 : end*/

             $(".searchArea .research").bind("click", function(e){
                           if(searchStatus != 1 ) {
                                        if(gnbTextStatus) {

                                        } else {
                                                     $(this).attr("value", "");
                                                     gnbTextStatus = 1;
                                                     
                                                     clearTimeout(valueTimer); //150127
                                        }
                           } else {
                                        renderSearchActive();


                           }
             })

             //$(".searchArea .research").width( $(".searchArea .research").width() -25 );

             $(".searchArea .research").bind("focusout", function(e){
                           if( $(this).attr("value")=="" ) {
                                        /*150127 : str*/
                                        //$(this).attr("value", "무엇을 찾고 계신가요?")
                                        valueTimer = setInterval(function(){
											autovalue();				
										}, valueSec);
				                      /*150127 : end */

                                        gnbTextStatus = 0;
                           } else {
                                        gnbTextStatus = 1;
                           }

                           $(".center-area").css({
                                        position:"relative"

                           });
                           renderGNBResize();
             })

             $(".searchBtn").bind("click", function(e){
                           if(searchStatus) {

                                        renderSearchActive();

                           } else {

                                        $("#SPORTS").val("");
                                        $("#COLOR").val("");
                                        $("#SLHT_CD").val("");
                                        $("#SIZES").val("");
                                        $("#BESTFOR").val("");
                                        $("#SORT_FIELD").val("");
                                        $('#pageIndex').val("0");
                                        fn_showSearchNavi();
                           }
             })
});

function renderDepthAddBtn() {
             $(".subnav-area").each(function(n){
                           //스포츠는 더보기버튼 제외한다.
                           if(n == 3) return;

                           var k = 0;

                           $(this).find(".subnav-box ul").each(function(o){
                                        var l = $(this).find("li").size();

                                        if(l > k) {
                                                     k = l;
                                        }
                           });

                           $(this).parents(".subnav").find(".subnav-btn-area").hide();
                           /*
                           if(k < 5) {



                                        var flag = $(this).parents(".subnav").attr("data-subnav-title") ;
                                        var uStr = flag.toUpperCase();
                                        arrSubBoxHei[uStr] = parseInt(arrSubBoxHei[uStr]) - 40;
                           }
                           */
             })
}

function renderSearchActive() {
             initialize();
             $(".tier0 .facet>a").removeClass("pushed");

             //일단 확장된 상태로 셋팅
             searchStatus = 0;

             var s = parseInt($(".gnav .left-area").width())
             var w = $(window).width();

             $(".center-area").css({
                           position:"fixed",
                           zIndex:1005,
                           left:s
             });

             $(".center-area").animate({
                           left: 0,
                           width:w
             },
             {
                           duration : du,
                           queue : false,
                           complete:function() {
                                        //isRun = false;
                                        $(".searchArea .research").focus()
                           }
             });

             $(".searchArea").fadeIn(200);

             var v = $(".searchArea .research").attr("value");
             if( v != "" && v != $(window).data("nowsearchvalue") && v !="찾기" ) { //150127

             } else {
                           $(".searchArea .research").attr("value", "");
             }
}

function renderGNBResize() {
             var v = $(".searchArea .research").attr("value");

             if( v != "" && v !=  $(window).data("nowsearchvalue") && v !="찾기" ) { //150127
                           gnbTextStatus = 1;
             } else {
                           gnbTextStatus = 0;
             }

             $(".center-area").css({
                           position:"relative"
             });

             //alert(  $(".gnav .left-area").width() + $(".gnav .right-area").width() );
             var wid = $(window).width() - (  $(".gnav .left-area").width() + $(".gnav .right-area").width() +65 );

             if(wid>150) {
                           if(gnbTextStatus) {

                           } else {
                                        $(".searchArea .research").attr("value", $(window).data("nowsearchvalue")); //150127
                           }
                           $(".center-area").width(wid);
                           $(".searchArea").fadeIn(200);

                           searchStatus = 0;
             } else if(wid>100) {
                           if(gnbTextStatus) {

                           } else {
                                        $(".searchArea .research").attr("value", "찾기");
                           }
                           $(".center-area").width(wid);

                           searchStatus = 1;
             } else if(wid>50) {
                           if(gnbTextStatus) {

                           } else {
                                        $(".searchArea .research").attr("value", "찾기");
                           }
                           $(".center-area").width(wid);
                           $(".searchArea").fadeIn(200);

                           searchStatus = 1;
             } else if(wid>30) {
                           if(gnbTextStatus) {

                           } else {
                                        $(".searchArea .research").attr("value", "찾기");
                           }
                           $(".center-area").width(wid);
                           $(".searchArea").fadeOut(200);

                           searchStatus = 1;
             } else {
                           $(".center-area").width(20);
                           $(".searchArea").fadeOut(200);

                           searchStatus = 1;
             }
}

$(function(){
             var recent_id = 0;
             var isRun = false;
             var du = 200;
             var size;

             function renderRecent() {
                           $(".tier0 .btn-recent-left").bind("click", function(e){
                                        e.preventDefault();

                                        var id = recent_id-1;
                                        setRecentItem(id);
                           });
                           $(".tier0 .btn-recent-right").bind("click", function(e){
                                        e.preventDefault();

                                        var id = recent_id+1;
                                        setRecentItem(id);
                           });
                           size = $(".tier0 .subnav.recent ul li").size();

                           $(".tier0 .btn-recent-left").hide();

                           if(size < 4) {
                                        $(".tier0 .btn-recent-right").hide();
                           }
             }

             function setRecentItem($id) {
                           if(isRun)
                                        return;
                           isRun = true;

                           var target = -102 * $id;

                           $(".tier0 .subnav.recent ul").stop().animate({
                                        "margin-left": target+"px"
                           },
                           {
                                        duration : du,
                                        queue : false,
                                        complete:function() {
                                                     isRun = false;
                                        }
                           });
                           renderRecentBtn($id);
                           recent_id = $id;
             }

             function renderRecentBtn($id) {
                           $(".tier0 .btn-recent-left").show();
                           $(".tier0 .btn-recent-right").show();

                           if($id < 1) {
                                        $(".tier0 .btn-recent-left").hide();
                                        $(".tier0 .btn-recent-right").show();
                           }

                           if($id  == size-3) {
                                        $(".tier0 .btn-recent-right").hide();
                           }
             }
             renderRecent();

             //renderFlvPlayer("SU12_Young_Athletes_Flex.flv","테스트입니다.");
})

function showDim() {
             $(".overLay").height( $(document).height() );
             $(".overLay").fadeIn(200);
}


function hideDim() {
             $(".overLay").fadeOut(200);
}

var img_path;

function renderFlvPlayer($path, $title) {
             $(".flv-player").show();
             showDim();
             swfobject.embedSWF(img_path+"/web/swf/Flv_Player.swf", "flvContent", 910, 510, "9.0.0", img_path+"/web/swf/expressInstall.swf",{ "path": $path , "title": $title },{"wmode":"transparent","allowFullScreen":"true","allowScriptAccess":"always"}, {});
}


function callBackClose() {
             $(".flv-player").hide();
             hideDim();
}


function imgPath(str) {
             img_path = str;
}


$(function(){

             if( $(".login #loginId_1").val() != "" ){
                           $(".id_text").hide();
             }

             if( $(".login #password_1").val() != "" ){
                           $(".pw_text").hide();
             }

             $(".login #loginId_1").on("focusin", function(e) {
                           $(".id_text").hide();
             });
             $(".login #loginId_1").on("focusout", function(e) {
                           if($(this).val() == "") {
                                        $(".id_text").show();
                           }
             });

             $(".login #password_1").on("focusin", function(e) {
                           $(".pw_text").hide();
             });

             $(".login #password_1").on("focusout", function(e) {
                           if($(this).val() == "") {
                                        $(".pw_text").show();
                           }
             });


             $(".id_text").on("click", function(e) {

                           $(".id_text").hide();
                           $(".login #loginId_1").focus();
             });

             $(".pw_text").on("click", function(e) {
                           $(".pw_text").hide();
                           $(".login #password_1").focus();
             });

         	/* 20140228 s */
         	$(".subnav-con").each(function(n){
         		if(n!=3) {

         			$(this).parent().css({
         				left:"50%",
         				top:"50px",
         				marginLeft: "-485px"//($(this).parent().width()/2) 20140228 수정

         			})
         		} else {

         			$(this).parent().css({
         				width:"100%",
         				top:"50px"
         			})
         		}
         	});
         	/* 20140228 e */

});






