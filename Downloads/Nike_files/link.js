//태그
var _tag = function(element,passTag){if(arguments.length == 1){return d.getElementsByTagName(arguments[0]);}else{if(typeof element == "string"){return _id(element).getElementsByTagName(passTag);}else{return element.getElementsByTagName(passTag);}}}


function product_selector(val,link,imgs){
	var wrap;
	var product_div;
	if(val.parentNode.parentNode.parentNode.parentNode.nodeName == "LI"){
		wrap =  val.parentNode.parentNode.parentNode.parentNode;
	}else{
		wrap =  val.parentNode.parentNode.parentNode.parentNode.parentNode;
	}
	if(_tag(wrap,"div")[0].className == "thumanail"){
		product_div = _tag(wrap,"div")[0];
	}
	_tag(product_div,"a")[0].href = link;
	_tag(product_div,"img")[0].src = imgs;
}

function fn_showGoodsDetail (pin) {
	var defaultProps = {
			goodsNo:'',
			colorOptionValueCode:'',
			displayNo:''/*,
			target:''*/
		};	
	pin = $.extend(defaultProps, pin||{});

	var uri = '/goods/showGoodsDetail.lecs?';
	var param = 'goodsNo=' + pin.goodsNo;
	
	if(pin.colorOptionValueCode != '') {
		param += '&colorOptionValueCode=' + pin.colorOptionValueCode;
	}
	if(pin.displayNo != '') {
		param += '&displayNo=' + pin.displayNo;
	}
//	if(pin.target != '') {
//		param += '&target=' + pin.target;
//	}
	
	fn_goPage(uri + param, pin.target);
	
}

function fn_showDisplayShop (pin) {	
	var defaultProps = {
			displayNo:'',
			target:''
		};
	pin = $.extend(defaultProps, pin||{});		
	
	var param = "displayNo=" + pin.displayNo;
	var uri = "/display/displayShop.lecs?" + param;
	
	fn_goPage(uri, pin.target);
	
};


//해당 URL로 이동
function fn_goPage (uri, target) {
	if(uri != '' && (typeof(target) == 'undefined' || target == '_self' || target == '')) {
		window.location.href = uri;
	} else if(uri != '' && (typeof(target) != 'undefined' && target == '_blank')) {
		window.open(uri, "", "");
	} else if(uri != '' && (typeof(target) != 'undefined' && target == '_top')) {
		top.window.location.href = uri;
	} else if(uri != '' && (typeof(target) != 'undefined' && target == '_parent')) {
		parent.window.location.href = uri;
	}
}