window.Modernizr = function (f, h, c) {
    function a(a, b) {
        for (var e in a) {
            var g = a[e];
            if (!~("" + g).indexOf("-") && d[g] !== c) return "pfx" == b ? g : !0
        }
        return !1
    }

    function b(b, e, g) {
        var d = b.charAt(0).toUpperCase() + b.slice(1),
            f = (b + " " + p.join(d + " ") + d).split(" ");
        if ("string" === typeof e || "undefined" === typeof e) e = a(f, e);
        else {
            f = (b + " " + q.join(d + " ") + d).split(" ");
            a: {
                b = f;
                for (var h in b)
                    if (d = e[b[h]], d !== c) {
                        e = !1 === g ? b[h] : "function" === typeof d ? d.bind(g || e) : d;
                        break a
                    }
                e = !1
            }
        }
        return e
    }
    var e = {},
        g = h.documentElement;
    f = h.createElement("modernizr");
    var d = f.style,
        p = ["Webkit", "Moz", "O", "ms"],
        q = ["webkit", "moz", "o", "ms"];
    f = {};
    h = [];
    var l = h.slice,
        j, m = {}.hasOwnProperty,
        k;
    "undefined" !== typeof m && "undefined" !== typeof m.call ? k = function (a, b) {
        return m.call(a, b)
    } : k = function (a, b) {
        return b in a && "undefined" === typeof a.constructor.prototype[b]
    };
    Function.prototype.bind || (Function.prototype.bind = function (a) {
        var b = this;
        if ("function" != typeof b) throw new TypeError;
        var e = l.call(arguments, 1),
            c = function () {
                if (this instanceof c) {
                    var d = function () {};
                    d.prototype = b.prototype;
                    var d = new d,
                        g = b.apply(d, e.concat(l.call(arguments)));
                    return Object(g) === g ? g : d
                }
                return b.apply(a, e.concat(l.call(arguments)))
            };
        return c
    });
    f.csstransitions = function () {
        return b("transition")
    };
    for (var n in f) k(f, n) && (j = n.toLowerCase(), e[j] = f[n](), h.push((e[j] ? "" : "no-") + j));
    e.addTest = function (a, b) {
        if ("object" == typeof a)
            for (var d in a) k(a, d) && e.addTest(d, a[d]);
        else {
            a = a.toLowerCase();
            if (e[a] !== c) return e;
            b = "function" == typeof b ? b() : b;
            enableClasses && (g.className += " " + (b ? "" : "no-") + a);
            e[a] = b
        }
        return e
    };
    d.cssText = "";
    return f = null, e._version = "2.6.1", e._domPrefixes = q, e._cssomPrefixes = p, e.testProp = function (b) {
        return a([b])
    }, e.testAllProps = b, e
}(this, this.document);
(function (f, h) {
    var c = function (a, b) {
        this.options = f.extend(!0, {}, this.defaults, b);
        this.container = a;
        this.dimensions = {
            height: this.container.height(),
            width: this.container.width()
        };
        this.slides = this.container.find("." + this.options.slideClass).each(f.proxy(function () {
            this.addSlide(this)
        }, this));
        this.numSlides = this.slides.length;
        this.currentSlide = 0;
        this.nextSlide = 1 < this.numSlides ? 1 : 0;
        this.previousSlide = this.lastSlide = 0 === this.numSlides ? null : this.numSlides - 1;
        this.status = {
            paused: !1,
            playing: !1,
            stopped: !0
        };
        this.isAnimating = !1;
        if (1 >= this.numSlides) this.disabled = !0;
        else return this.on("transitionComplete", function () {
            this.isAnimating = !1
        }), this.on("play", this.options.onPlay), this.on("stop", this.options.onStop), this.on("pause", this.options.onPause), this.on("restart", this.options.onRestart), this.on("transitionStart", this.options.transition.onStart), this.on("transitionComplete", this.options.transition.onComplete), "function" === typeof this.options.transition.onFinish && (console.warn("The options.transition.onFinish property has been deprecated and will be removed in future versions. Please use options.transition.onComplete to aviod breakage. Love Revolver.js."), this.on("transitionComplete", this.options.transition.onFinish)), f.proxy(this.options.onReady, this)(), this.options.autoPlay && this.play({}, !0), this
    };
    c.prototype.defaults = {
        autoPlay: !0,
        onReady: function () {},
        onPlay: function () {},
        onStop: function () {},
        onPause: function () {},
        onRestart: function () {},
        rotationSpeed: 4E3,
        slideClass: "slide",
        transition: {
            easing: "swing",
            onStart: function () {},
            onFinish: !1,
            onComplete: function () {},
            speed: 500,
            type: "fade"
        }
    };
    c.prototype.previousSlide = null;
    c.prototype.currentSlide = null;
    c.prototype.nextSlide = null;
    c.prototype.numSlides = 0;
    c.prototype.lastSlide = null;
    c.prototype.container = null;
    c.prototype.slides = [];
    c.prototype.iteration = 0;
    c.prototype.intervalId = null;
    c.prototype.status = null;
    c.prototype.options = null;
    c.prototype.dimensions = null;
    c.prototype.isAnimating = null;
    c.prototype.disabled = !1;
    c.prototype.VERSION = "1.0.5";
    c.prototype.addSlide = function (a) {
        this.slides.push(a)
    };
    c.prototype.changeStatus = function (a) {
        var b = this;
        f.each(this.status, function (e) {
            b.status[e] = e === a
        });
        return this
    };
    c.prototype.transition = function (a) {
        if (!1 === this.disabled && !1 === this.isAnimating) {
            a = f.extend(!0, {}, this.options.transition, a);
            var b = f.proxy(this.transitions[a.type], this);
            this.isAnimating = !0;
            b(a);
            this.currentSlide = this.nextSlide;
            this.previousSlide = 0 === this.currentSlide ? this.lastSlide : this.currentSlide - 1;
            this.nextSlide = this.currentSlide === this.lastSlide ? 0 : this.currentSlide + 1;
            this.iteration++;
            this.trigger("transitionStart")
        }
        return this
    };
    c.prototype.transitions = {
        none: function () {
            this.slides.eq(this.currentSlide).hide();
            this.slides.eq(this.nextSlide).show();
            this.trigger("transitionComplete")
        },
        reveal: function (a) {
            this.slides.eq(this.nextSlide).css({
                width: 0,
                height: this.dimensions.height,
                "z-index": this.iteration + 1
            }).show().animate({
                width: this.dimensions.width
            }, a.speed, a.easing, this.trigger.bind(this, "transitionComplete"));
            return this
        }
    };
    c.prototype.play = function (a, b) {
        !1 === this.disabled && !this.status.playing && (this.changeStatus("playing"), this.trigger("play"), b || this.transition(a), this.intervalId = setInterval(f.proxy(this.transition, this), parseFloat(this.options.rotationSpeed)));
        return this
    };
    c.prototype.pause = function () {
        !1 === this.disabled && !this.status.paused && (this.changeStatus("paused"), this.trigger("pause"), null !== this.intervalId && (clearInterval(this.intervalId), this.intervalId = null));
        return this
    };
    c.prototype.stop = function () {
        !1 === this.disabled && !this.status.stopped && (this.changeStatus("stopped"), this.trigger("stop"), null !== this.intervalId && (clearInterval(this.intervalId), this.intervalId = null));
        return this.reset()
    };
    c.prototype.reset = function () {
        0 !== this.currentSlide && (this.nextSlide = 0);
        return this
    };
    c.prototype.restart = function (a) {
        if (!0 === this.disabled) return this;
        this.trigger("restart");
        return this.stop().play(a)
    };
    c.prototype.first = function (a) {
        return this.goTo(0, a)
    };
    c.prototype.previous = function (a) {
        return this.goTo(this.previousSlide, a)
    };
    c.prototype.goTo = function (a, b) {
        a = parseInt(a);
        if (!0 === this.disabled || a === this.currentSlide) return this;
        this.nextSlide = a;
        return !this.status.playing ? this.transition(b) : this.pause().play(b)
    };
    c.prototype.next = function (a) {
        return this.goTo(this.nextSlide, a)
    };
    c.prototype.last = function (a) {
        return this.goTo(this.lastSlide, a)
    };
    c.prototype.on = function (a, b) {
        return this.container.on(a + ".revolver", f.proxy(b, this))
    };
    c.prototype.off = function (a, b) {
        return this.container.off(a + ".revolver", f.proxy(b, this))
    };
    c.prototype.trigger = function (a) {
        return this.container.trigger(a + ".revolver")
    };
    c.prototype.transitions.fade = function (a) {
        var b, e, c, d;
        b = this;
        e = {
            swing: "ease-in-out",
            linear: "linear"
        }[a.easing];
        c = this.slides.eq(this.nextSlide);
        d = this.slides.eq(this.currentSlide);
        d.css("z-index", this.numSlides);
        c.css("z-index", this.nextSlide);
        c.show(0).promise().done(function () {
            Modernizr.csstransitions ? (d.css({
                opacity: 0,
                transition: "opacity " + a.speed / 1E3 + "s " + e
            }), setTimeout(function () {
                d.hide().css({
                    opacity: 1,
                    transition: "opacity 0s " + e
                });
                b.trigger("transitionComplete")
            }, a.speed)) : d.fadeOut(a.speed, a.easing, function () {
                b.trigger("transitionComplete")
            })
        })
    };
    c.prototype.transitions.slide = function (a) {
        return f.proxy(this.transitions.slide[a.direction], this)(a)
    };
    c.prototype.defaults.transition.direction = "left";
    c.prototype.transitions.slide.left = function (a) {
        var b, e, c, d;
        b = this.slides.eq(this.currentSlide);
        e = {
            swing: "ease-in-out",
            linear: "linear"
        }[a.easing];
        c = this.slides.eq(this.nextSlide);
        d = this;
        c.css({
            left: d.dimensions.width,
            top: 0,
            transition: "left 0s " + e
        }).show(0).promise().done(function () {
            c.css("z-index", this.numSlides);
            b.css("z-index", this.currentSlide);
            Modernizr.csstransitions ? (b.css({
                left: 0 - d.dimensions.width,
                top: 0,
                transition: "left " + a.speed / 1E3 + "s " + e
            }), c.css({
                top: 0,
                left: 0,
                transition: "left " + a.speed / 1E3 + "s " + e
            }), setTimeout(function () {
                b.hide();
                d.trigger("transitionComplete")
            }, a.speed)) : (b.stop(!0).animate({
                left: 0 - d.dimensions.width,
                top: 0
            }, a.speed, a.easing, function () {
                f(this).hide()
            }), c.stop(!0).animate({
                top: 0,
                left: 0
            }, a.speed, a.easing, function () {
                this.trigger("transitionComplete")
            }))
        })
    };
    c.prototype.transitions.slide.right = function (a) {
        var b, c, g, d;
        b = this.slides.eq(this.currentSlide);
        c = {
            swing: "ease-in-out",
            linear: "linear"
        }[a.easing];
        g = this.slides.eq(this.nextSlide);
        d = this;
        g.css({
            left: 0 - d.dimensions.width,
            top: 0,
            transition: "left 0s " + c
        }).show(0).promise().done(function () {
            g.css("z-index", this.numSlides);
            b.css("z-index", this.currentSlide);
            Modernizr.csstransitions ? (b.css({
                left: d.dimensions.width,
                top: 0,
                transition: "left " + a.speed / 1E3 + "s " + c
            }), g.css({
                top: 0,
                left: 0,
                transition: "left " + a.speed / 1E3 + "s " + c
            }), setTimeout(function () {
                b.hide();
                d.trigger("transitionComplete")
            }, a.speed)) : (b.stop(!0).animate({
                left: d.dimensions.width,
                top: 0
            }, a.speed, a.easing, function () {
                f(this).hide()
            }), g.stop(!0).animate({
                top: 0,
                left: 0
            }, a.speed, a.easing, function () {
                this.trigger("transitionComplete")
            }))
        })
    };
    c.prototype.transitions.slide.up = function (a) {
        var b, c, g, d;
        b = this.slides.eq(this.currentSlide);
        c = {
            swing: "ease-in-out",
            linear: "linear"
        }[a.easing];
        g = this.slides.eq(this.nextSlide);
        d = this;
        g.css({
            left: 0,
            top: d.dimensions.height,
            transition: "top 0s " + c
        }).show(0).promise().done(function () {
            g.css("z-index", this.numSlides);
            b.css("z-index", this.currentSlide);
            Modernizr.csstransitions ? (b.css({
                left: 0,
                top: 0 - d.dimensions.height,
                transition: "top " + a.speed / 1E3 + "s " + c
            }), g.css({
                top: 0,
                left: 0,
                transition: "top " + a.speed / 1E3 + "s " + c
            }), setTimeout(function () {
                b.hide();
                d.trigger("transitionComplete")
            }, a.speed)) : (b.stop(!0).animate({
                left: 0,
                top: 0 - d.dimensions.height
            }, a.speed, a.easing, function () {
                f(this).hide()
            }), g.stop(!0).animate({
                top: 0,
                left: 0
            }, a.speed, a.easing, function () {
                this.trigger("transitionComplete")
            }))
        })
    };
    c.prototype.transitions.slide.down = function (a) {
        var b, c, g, d;
        b = this.slides.eq(this.currentSlide);
        c = {
            swing: "ease-in-out",
            linear: "linear"
        }[a.easing];
        g = this.slides.eq(this.nextSlide);
        d = this;
        g.css({
            left: 0,
            top: 0 - d.dimensions.height,
            transition: "top 0s " + c
        }).show(0).promise().done(function () {
            g.css("z-index", this.numSlides);
            b.css("z-index", this.currentSlide);
            Modernizr.csstransitions ? (b.css({
                left: 0,
                top: d.dimensions.height,
                transition: "top " + a.speed / 1E3 + "s " + c
            }), g.css({
                top: 0,
                left: 0,
                transition: "top " + a.speed / 1E3 + "s " + c
            }), setTimeout(function () {
                b.hide();
                d.trigger("transitionComplete")
            }, a.speed)) : (b.stop(!0).animate({
                left: 0,
                top: d.dimensions.height
            }, a.speed, a.easing, function () {
                f(this).hide()
            }), g.stop(!0).animate({
                top: 0,
                left: 0
            }, a.speed, a.easing, function () {
                this.trigger("transitionComplete")
            }))
        })
    };
    h.Revolver = c;
    f.fn.revolver = function (a) {
        return this.each(function () {
            f.data(this, "revolver") || f.data(this, "revolver", new c(f(this), a))
        })
    }
})(jQuery, this);