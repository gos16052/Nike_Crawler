/****************************************/
/*	Name: Nike Store
/*	Part: makeCookieInfluxChannel.js
/*	Version: 1.0
/*	Author: medic
/****************************************/

var FOR_GEN_INFLUX_CHANNEL_NO = "influx_channel_no";
var FOR_GEN_INFLUX_CHANNEL_DETAIL_NO = "influx_channel_detail_no";
var FOR_GEN_AJAX_URL = "/display/chkLoginAjax.lecs?catalYn=Y&"+FOR_GEN_INFLUX_CHANNEL_NO+"=";

function getHtmlGenParameter(strParamName) {
	var arrResult = null;
	try{
		var onlyParam =  location.search;
		if(onlyParam.indexOf("?") > -1){
			onlyParam =  onlyParam.replace('?', ''); 
			//alert('onlyParam :'+onlyParam);
			var params = onlyParam.split('&');
			for(var i=0; i<params.length; i++){
				var param = params[i].split('=');
				if(strParamName == param[0]){
					if(param[1].indexOf("#") >-1){
						arrResult = param[1].split('#')[0];
						return arrResult ;
					}else{
						arrResult = param[1];
						return arrResult ;
					}
				}
			}
		}
		return arrResult ;

	}catch(e){
		return arrResult;
	}
}

try{
	var forGen_channelNo = getHtmlGenParameter(FOR_GEN_INFLUX_CHANNEL_NO)==null?'':getHtmlGenParameter(FOR_GEN_INFLUX_CHANNEL_NO);
	var forGen_channelDetailNo =  getHtmlGenParameter(FOR_GEN_INFLUX_CHANNEL_DETAIL_NO)==null?'':getHtmlGenParameter(FOR_GEN_INFLUX_CHANNEL_DETAIL_NO);
	//alert("InfluxChannelNo: "+ forGen_channelNo +"  DetailNo: "+ forGen_channelDetailNo);
	
		var ajaxUrl = FOR_GEN_AJAX_URL+forGen_channelNo;
		if ( forGen_channelDetailNo != ''  ) {			
			ajaxUrl = ajaxUrl + "&"+FOR_GEN_INFLUX_CHANNEL_DETAIL_NO+"="+forGen_channelDetailNo;
		}
		$.get(ajaxUrl, '' , function(data) {});

}catch(e){
}
