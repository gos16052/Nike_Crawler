/**
 * Utility for submitting to iframe and getting result by callback function
 * 
 * [Usage]
 *   1. Client side
 *     a. Make callback function. 'result' parameter is returned object from server.
 *       ex)
 *         function handleSave(result) {
 *           if(reuslt.success) {
 *             // do your job
 *           } else if(result.message) {
 *             alert(result.message);
 *           } else {
 *             alert("Failed your job");
 *           }
 *         }
 *
 *     b. Call IframeSubmitter.submit().
 *       ex)
 *       function doSubmit(form) {
 *         IframeSubmitter.submit(form, "handleSave", "submitIframe");
 *       }
 *       
 *   2. Server side
 *     a. Set request attribute with key 'result' with string value as javascript object format.
 *       ex)
 *         String resultObject = "{success: true, message: null, value1: "ABC", value2: 123, ...}";
 *         request.setAttribute("result", resultObject);
 *         
 *     b. Map response page to "iframeSubmitResult.jsp"
 */
var IframeSubmitter = {
    /** Reference to callback function to be called */
    callback: null,
    
    /** Result object from server */
    result: null,
    
    /** If true, 'result' object is traced in iframe window */
    debugEnabled: true,
    
    /**
     * Sumit form
     * 
     * @param form form to be submitted
     * @param callback callback function to be called when response comes to iframe
     * @param iframeName target iframe name
     * @param debugEnabled (Optional) If true, 'result' object is traced in iframe window
     */
    submit: function(form, callback, iframeName, debugEnabled) {
        this.callback = null;
        this.result = null;
        
        var callbackTag = null;
        for (var i = 0; i < form.length; i++) {
        	if (form.elements[i].name == 'callback') {
        		callbackTag = form.elements[i];
        	}
        }
        
        if ( callbackTag && callbackTag.tagName.toLowerCase() == "input" && callbackTag.type.toLowerCase() == "hidden" ) {
            callbackTag.value = callback;
        } else {
            callbackTag = document.createElement("INPUT");
            callbackTag.type = "hidden";
            callbackTag.name = "callback";
            callbackTag.value = callback;
            form.appendChild(callbackTag);
        }

        // Used when request is redirected to error pages
        var alertOnErrorPageTag = form.alertOnErrorPageTag;
        if ( alertOnErrorPageTag && alertOnErrorPageTag.tagName.toLowerCase() == "input" && alertOnErrorPageTag.type.toLowerCase() == "hidden" ) {
        	alertOnErrorPageTag.value = "Y";
        }
        else {
	        alertOnErrorPageTag = document.createElement("INPUT");
	        alertOnErrorPageTag.type = "hidden";
	        alertOnErrorPageTag.name = "alertOnErrorPage";
	        alertOnErrorPageTag.value = "Y";
	        form.appendChild(alertOnErrorPageTag);
        }
        
        this.debugEnabled = debugEnabled;
        
        form.target = iframeName;
        form.submit();
    },
    
    /**
     * Executes callback, called by iframe - this method do not really calls callback, but sets up timer to call it
     * 
     * @param iframeWindow iframe window getting response
     * @param callback callback function to be called
     * @param result result object from server
     */
    executeCallback: function(iframeWindow, callback, result) {
        this.callback = callback;
        this.result = result;
        
        setTimeout(this.doExecuteCallback, 100);
        
        if(!this.debugEnabled) {
            iframeWindow.location.replace("about:blank");
        }
    },
    
    /**
     * Execute callback really
     */
    doExecuteCallback: function() {
        if(IframeSubmitter.callback && IframeSubmitter.result) {
            IframeSubmitter.callback(IframeSubmitter.result);
        }
    }
}
