
/**
 * ajax call 
 * id : grid id
 * type : post , get
 * url : ajax url
 * data : 전달 파라미터
 * dataType : return data(text, json, html, xml)
 * callBack : call back function 
 */
function ajaxCall(type, url, data, dataType, callBack) {
	$.ajax({
		type: type,
		url: url,
		data: data,
		dataType:dataType,			  
		success: function(data) {
			callBack(data);			
		}
	});
}

/**
 * ajax call (example)
 *
 * id : 그리드 id 값
 * cdGrpId : 코드 그룹 값
 */
function ajaxCallSample(id, cdGrpId) {
	ajaxCall("post", "/goods/listCode.do", "cdGrpId="+cdGrpId, "json", callback = function(data){
		$(data).each(function(){
			alert(this.codeName);
		});
	});
}
